<?php
/**
 * 消息队列配置
 * 内置驱动：redis、database、topthink、sync
 */

return [
    //Redis驱动
    'connector' => 'redis',
    "expire"=>60,//任务过期时间默认为秒，禁用为null
    "default"=>"default",//默认队列名称
    "host"=>"127.0.0.1",//Redis主机IP地址
    "port"=>6379,//Redis端口
    "password"=>"Owen.123tx",//Redis密码
    "select"=>5,//Redis数据库索引
    "timeout"=>0,//Redis连接超时时间
    "persistent"=>false,//是否长连接
];