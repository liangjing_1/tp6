<?php
/**
 * 自动推单
 */

namespace app\command;

use app\lucky\pay\service\UserGoldService;
use app\lucky\push\service\PushService;

class PushTest extends \think\console\Command
{
    protected function configure()
    {
        $this->setName('push_test')->setDescription('推送');
    }

    protected function execute(\think\console\Input $input, \think\console\Output $output)
    {
        \think\facade\Log::record('自动推单开始', 'business');
        $service = new PushService();
        $service->push();
        \think\facade\Log::record('自动推单结束', 'business');
    }
}