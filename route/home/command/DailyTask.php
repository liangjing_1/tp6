<?php


namespace app\command;

use app\lucky\pay\service\UserGoldService;
use app\lucky\common\InstanceTrait;

class DailyTask extends \think\console\Command
{
    protected function configure()
    {
        $this->setName('DailyTask')->setDescription('掌上体育每日任务重置');
    }

    protected function execute(\think\console\Input $input, \think\console\Output $output)
    {
        \think\facade\Log::record('掌上体育每日任务重置开始', 'business');
        UserGoldService::getInstance()->delEverydayTask();
        \think\facade\Log::record('掌上体育每日任务重置结束', 'business');
    }
}