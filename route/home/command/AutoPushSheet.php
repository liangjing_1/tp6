<?php
/**
 * 自动推单
 */

namespace app\command;

use app\lucky\pay\service\UserGoldService;

class AutoPushSheet extends \think\console\Command
{
    protected function configure()
    {
        $this->setName('autoPushSheet')->setDescription('自动推单');
    }

    protected function execute(\think\console\Input $input, \think\console\Output $output)
    {
        \think\facade\Log::record('自动推单开始', 'business');
        \app\sports\article\service\AutoPushSheet::getInstance()->exec();
        \think\facade\Log::record('自动推单结束', 'business');
    }
}