<?php
/**
 * 极光推送
 */
namespace app\common;

use JPush\Client;

class JPush
{
    private $key = '';
    private $secret = '';

    use InstanceTrait;

    public function _init()
    {
        $this->key = '34a992763fc1ae54c400cacf';
        $this->secret = '2c5c4fc380dd8a608358b4ff';
    }

    /**
     * 指定注册ID发送
     */
    public function push($registrationId, $title, $text, $url, $itemId)
    {
        $registrationId = array_filter($registrationId);
        \Log::info(var_export([$registrationId, $title, $text, $url, $itemId], true));
        $this->_init();

        $client = new Client($this->key, $this->secret);
        $push = $client->push();

        $push->setPlatform('all');
        $push->addRegistrationId($registrationId);
        $push->addAndroidNotification($title, $text);
        $push->androidNotification($text, ['title' => $title, 'alert_type' => 2, 'extras' => ['url' => $url, 'id' => $itemId]]);
        $push->iosNotification(['title' => $title, 'body' => $text], ['extras' => ['url' => $url, 'id' => $itemId]]);
        $push->send();
    }
}