<?php
/**
 * 自动推单
 */

namespace app\sports\article\service;

use app\common\InstanceTrait;
use app\lucky\admin\model\ExpertUserV2Model;
use app\sports\admin\model\PushSheetModel;
use app\sports\match\model\JCModel;
use app\sports\match\service\SportsApiService;

class AutoPushSheet
{
    use InstanceTrait;

    private $jcData = [];
    private $bdData = [];

    /**
     * 执行
     */
    public function exec()
    {
        //获取当前的小时和分钟
        $hour   = date('H');
        $minute = date('i');

        //获取配置
        $autoPushConfig = ExpertUserV2Model::getInstance()->getAllAutoPushSheetConfig();
        if (empty($autoPushConfig)) {
            return true;
        }

        //添加推单
        foreach ($autoPushConfig as $value) {
            $ext = json_decode($value['ext'], true);
            if (is_array($ext)) {
                foreach ($ext as $v) {
                    if ($v['hour'] == $hour && $v['minute'] == $minute && $value['type'] == 1) {
                        $this->addSpfPushSheet($value['expert_user_id']);
                    }

                    if ($v['hour'] == $hour && $v['minute'] == $minute && $value['type'] == 2) {
                        $this->addBdPushSheet($value['expert_user_id']);
                    }
                }
            }
        }
    }

    /**
     * 推单(胜平负)
     */
    private function addSpfPushSheet($expertUserId)
    {
        $jcData = $this->getJcData();
        if (empty($jcData)) {
            \Log::error('自动推单 胜平负竞猜无数据');
            return false;
        }

        $matchId = $jcData['match_id'];

        $pushSheetModel = new PushSheetModel();

        $shortHome = $jcData['short_home'];
        $shortAway = $jcData['short_away'];

        //移除第一个元素
        array_shift($jcData['spf_arr']);
        array_shift($jcData['rq_arr']);

        //取胜率最低的进行推单
        $spf = min($jcData['spf_arr']);
        $rq  = min($jcData['rq_arr']);

        if ($spf < $rq) {
            $key                = array_search($spf, $jcData['spf_arr']) + 1;
            $matchResult['spf'] = $key;
        } else {
            $key               = array_search($rq, $jcData['rq_arr']) + 1;
            $matchResult['rq'] = $key;
        }

        $title = "竞足单关推荐,跟上节奏 {$shortHome} VS {$shortAway}";

        $result = $pushSheetModel->addPushSheetService($expertUserId, $matchId, $matchResult, $title, '', 1);
        if (empty($result)) {
            \Log::info($expertUserId . ':胜平负自动推单失败');
            return false;
        }

        \Log::info($expertUserId . ':胜平负自动推单成功');
    }

    /**
     * 推单(北单)
     */
    private function addBdPushSheet($expertUserId)
    {
        $jcData = $this->getBdData();
        if (empty($jcData)) {
            \Log::error('自动推单 北单竞猜无数据');
            return false;
        }

        $matchId        = $jcData['match_id'];
        $pushSheetModel = new PushSheetModel();

        $home  = $jcData['home'];
        $away  = $jcData['away'];
        $title = "重磅归来，稳定推荐 {$home} VS {$away}";

        //胜率 胜和负
        $win               = $jcData['odds']['sf']['sf3'];
        $loss              = $jcData['odds']['sf']['sf0'];

        if ($win > $loss) {
            $matchResult['bd'] = 1;
        } else {
            $matchResult['bd'] = 2;
        }
        $result            = $pushSheetModel->addPushSheetService($expertUserId, $matchId, $matchResult, $title, '', 2);
        if (empty($result)) {
            \Log::info($expertUserId . ':北单自动推单失败');
            return false;
        }

        \Log::info($expertUserId . ':北单自动推单成功');
    }

    /**
     * 获取竞猜比赛
     */
    private function getJcData()
    {
        if ($this->jcData) {
            return array_rand($this->jcData, 1);
        }

        $service = new SportsApiService();
        $type    = JCModel::JC_FOOT_BALL_TYPE;

        $this->jcData = $service->getJcList($type, 1, 10);
        $this->jcData = isset($this->jcData['data']) && isset($this->jcData['data']['list']) ? $this->jcData['data']['list'] : [];

        $randKey = array_rand($this->jcData, 1);
        return $this->jcData[$randKey];
    }

    /**
     * 获取北单比赛
     */
    private function getBdData()
    {
        if ($this->bdData) {
            return array_rand($this->bdData, 1);
        }
        $service = new SportsApiService();
        $sportId = JCModel::BD_SPORT_ID_FOOT;

        $this->bdData = $service->getDbList($sportId, 1, 10);

        $randKey = array_rand($this->bdData, 1);
        return $this->bdData[$randKey];
    }
}