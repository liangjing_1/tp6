<?php
/**
 * 实名认证
 */

namespace app\sports\admin\controller;

use app\lucky\admin\controller\AdminBase;
use app\sports\admin\service\CertificationService;
use app\common\DataValidate;
use app\sports\certification\model\CertificationModel;
use app\sports\certification\validate\CertificationValidate;
use think\App;
use think\Request;

class Certification extends AdminBase
{

    public $service = null;
    public $sys = '';

    public function __construct(App $app = null, Request $request = null)
    {
        parent::__construct($app, $request);
        $this->service = new certificationService();
        $this->sys = \session('sys') ? \session('sys') : 343;
    }

    /**
     * @description  列表
     * 路由：可访问 /admin/certification/list
     */
    public function getList()
    {
        $postData = $this->request->post();
        $valiData = DataValidate::getInstance()->checkPager($postData);
        if ($valiData["code"] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($valiData["code"], $valiData["msg"]);
        }

        $postData = $valiData['data'];
        $data = $this->service->getList($postData, $this->sys);

        $this->_success($data);
    }

    /**
     * @description 审核
     * 路由：可访问 /admin/certification/audit
     */
    public function audit()
    {
        $postData = $this->request->post();
        $valiData = certificationValidate::getInstance()->checkUpdate($postData);
        if ($valiData["code"] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($valiData["code"], $valiData["msg"]);
        }

        $postData = $valiData['data'];
        $postData['admin_id'] = $this->loginInfo['admin_id'];

        $result = $this->service->audit($postData['id'], $postData, $this->sys);

        if ($result['code'] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($result['code'], $result["msg"]);
        }
        $this->_success($result["data"], $result["msg"]);
    }

    /**
     * @description  删除
     * 路由：可访问 /admin/certification/delete
     */
    public function delete()
    {
        $post = $this->request->post();
        if (!isset($post["id"])) {
            $this->_error(_MSG_INVALID_CLIENT_PARAM, "必须提供 certification_id");
        }

        $result = $this->service->delete($post["id"], $this->sys);

        if (!$result) {
            $this->_error(_MSG_NOT_ALLOWED_METHOD, "删除失败");
        }
        $this->_success([], "删除成功");
    }

    /**
     * @description  详情
     * 路由：可访问 /admin/certification/info
     */
    public function getInfo()
    {
        $post = $this->request->post();
        if (!isset($post["id"])) {
            $this->_error(_MSG_INVALID_CLIENT_PARAM, "必须提供 certification_id");
        }

        $result = $this->service->getInfo($post["id"], $this->sys);

        if ($result['code'] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($result['code'], $result['msg']);
        }
        $this->_success($result["data"], $result["msg"]);
    }


}
