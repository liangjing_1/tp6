<?php
/**
 * 禁言
 */

namespace app\sports\admin\service;

use app\common\DataService;
use app\common\Redis;
use app\hook\adminLog\context\AdminLogContext;
use app\hook\adminLog\controller\AdminLog;
use app\lucky\user\model\UserV2Model;
use app\lucky\user\service\UserService;
use app\sports\disablesendmsg\context\DisablesendmsgListContext;
use app\sports\disablesendmsg\model\DisablesendmsgModel;

class DisablesendmsgService
{
    //表名
    const ADMIN_OP_TABLE_NAME = 'user_disablesendmsg';

    public $model = '';

    public function __construct()
    {
        $this->model = new DisablesendmsgModel();
    }

    /**
     * 禁言时长数组
     * @return array
     */
    public function getDisablesendmsgDuration()
    {
        $result = [
            ['key' => 1, 'name' => '1天'],
            ['key' => 7, 'name' => '7天'],
            ['key' => 15, 'name' => '15天'],
            ['key' => 30, 'name' => '30天'],
            ['key' => 365, 'name' => '365天'],
            ['key' => 0, 'name' => '永久']
        ];
        return $result;
    }

    /**
     * 查询列表
     *
     * @access public
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function getList(DisablesendmsgListContext $context, $sys = 343)
    {
        $context->page_no = isset($context->page_no) ? $context->page_no : 1;
        $context->page_count = isset($context->page_count) ? $context->page_count : 10;
        //来源app1后台2
        $context->sourceId = isset($context->sourceId) ? $context->sourceId : 1;
        $context->operatorId = isset($context->operatorId) ? $context->operatorId : 0;
        $context->user_nickname = isset($context->user_nickname) ? $context->user_nickname : '';
        $context->isDel = isset($context->isDel) ? $context->isDel : 0;
        if ($context->user_nickname) {
            $userIdArr = UserService::getInstance()->getUserInfoByLikeUserNickname($context->user_nickname, $sys);
            $context->userIdArr = $userIdArr;
        }

        $result = $this->model->selectStatusListByPager($context, $sys);

        $data = $result['data'];

        //处理前端数据格式
        if ($data) {
            foreach ($data as $k => $v) {
                $userInfo = UserService::getInstance()->getUserInfo($v['user_id'], $sys);
                $data[$k]['nickname'] = isset($userInfo['nickname']) ? $userInfo['nickname'] : '';
                $data[$k]['headimgurl'] = isset($userInfo['headimgurl']) ? $userInfo['headimgurl'] : '';

                if ($context->sourceId == 1) {
                    //来源app1后台2 app时间格式
                    $data[$k]['disablesendmsg_date'] = DataService::getInstance()->_handleCreateTimeTwo(date('Y-m-d H:i:s', $v['disablesendmsg_time']));
                } else {
                    //后台时间格式
                    $data[$k]['disablesendmsg_date'] = date('Y-m-d H:i', $v['disablesendmsg_time']);
                }
            }
        }

        $pageTotal = (int)ceil($result['total'] / $context->page_count);
        $page_info = [
            'total' => $result['total'],//总记录
            'page_no' => $context->page_no,//页数
            'page_count' => $context->page_count,//每页显示数量
            'page_total' => $pageTotal,//总页数
        ];

        return [
            'page_info' => $page_info,
            'list' => $data,
        ];
    }

    /**
     * 添加禁言前页面数据
     * @access public
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function addBeforeInfo($userId, $sys = 343)
    {
        $userData = UserV2Model::getInstance()->getUserByUserId($userId, $sys);
        if ($userData["code"] != _MSG_SYSTEM_SUCCESS) {
            return [];
        }

        $data['userInfo'] = array("user_id" => $userData['data']['id'],
            "nickname" => $userData['data']['nickname'],
            "headimgurl" => $userData['data']['headimgurl']);

        //禁言时长数组
        $data['disablesendmsgDurationArr'] = $this->getDisablesendmsgDuration();

        return $data;
    }

    /**
     * 添加禁言
     *
     * @access public
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function addDisablesendmsg($data, $sys = 343)
    {
        $opInfo = '设为禁言';

        //防止重复添加
        $repetitionData = $this->model->getOneByUserId($data['user_id'], $sys);

        if ($repetitionData) {
            $updateData = [
                "operator_id" => isset($data['operator_id']) ? $data['operator_id'] : 0,
                "source_id" => isset($data['source_id']) ? $data['source_id'] : 1,
                "is_jiechu" => isset($data['is_jiechu']) ? $data['is_jiechu'] : 0,
                "disablesendmsg_reason" => isset($data['disablesendmsg_reason']) ? $data['disablesendmsg_reason'] : "",
                "disablesendmsg_duration" => isset($data['disablesendmsg_duration']) ? $data['disablesendmsg_duration'] : "",
                'disablesendmsg_time' => time(),
                'update_time' => time(),
                "sys" => $sys
            ];

            $result = $this->model->updateByUserId($updateData, $data['user_id'], $sys);
            if ($result) {
                //添加操作日志
                $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_UPDATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $repetitionData['id'], $opInfo);
                \think\facade\Hook::listen('admin_log', $context);

                $key = $this->_getDisablesendmsgUserListKey($sys);
                Redis::getInstance()->redisZadd($key, time(), $data['user_id']);
            }

            return $result;
        } else {
            $data = [
                "operator_id" => isset($data['operator_id']) ? $data['operator_id'] : 0,
                "user_id" => isset($data['user_id']) ? $data['user_id'] : 0,
                "source_id" => isset($data['source_id']) ? $data['source_id'] : 1,
                "disablesendmsg_reason" => isset($data['disablesendmsg_reason']) ? $data['disablesendmsg_reason'] : "",
                "disablesendmsg_duration" => isset($data['disablesendmsg_duration']) ? $data['disablesendmsg_duration'] : "",
                'disablesendmsg_time' => time(),
                "sys" => $sys
            ];

            $result = $this->model->insertGetId($data, $sys);

            if ($result) {
                //用户的禁言列表
                //将被禁言的用户id 添加到redis/ key=Disablesendmsg:343:user
                $key = $this->_getDisablesendmsgUserListKey($sys);
                Redis::getInstance()->redisZadd($key, time(), $data['user_id']);

                //添加操作日志
                $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_CREATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $result, $opInfo);
                \think\facade\Hook::listen('admin_log', $context);

                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 修改禁言
     *
     * @access public
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function editDisablesendmsg($disablesendmsgId, $data, $sys = 343)
    {
        $disablesendmsgInfo = $this->model->getOneById($disablesendmsgId, $sys);
        if (isset($disablesendmsgInfo['user_id'])) {
            $userId = $disablesendmsgInfo['user_id'];
        } else {
            return false;
        }

        $data = [
            "operator_id" => isset($data['operator_id']) ? $data['operator_id'] : 0,
            "source_id" => isset($data['source_id']) ? $data['source_id'] : 1,
            "id" => $data['id'],
            "is_jiechu" => isset($data['is_jiechu']) ? $data['is_jiechu'] : 0,
            'update_time' => time(),
            "sys" => $sys
        ];

        if ($data['is_jiechu']){
            $opInfo = '解除禁言';
        }else{
            $opInfo = '设为禁言';
        }

        $result = $this->model->update($disablesendmsgId, $data, $sys);

        if ($result) {
            //is_jiechu=0禁言 1解除
            if ($data['is_jiechu'] == 0) {
                //添加redis禁言用户列表
                $key = $this->_getDisablesendmsgUserListKey($sys);
                Redis::getInstance()->redisZadd($key, time(), $userId);
            } else {
                //将用户删除redis禁言用户列表
                $key = $this->_getDisablesendmsgUserListKey($sys);
                Redis::getInstance()->redisZrem($key, $userId);
            }

            //添加操作日志
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_UPDATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $disablesendmsgInfo['id'], $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return $result;
        } else {
            return false;
        }
    }

    /**
     * 修改禁言 通过userid
     *
     * @access public
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return bool 返回类型
     */
    public function editDisablesendmsgByUserId($data, $userId, $sys = 343)
    {
        $disablesendmsgInfo = $this->model->getOneByUserId($userId, $sys);
        if (!$disablesendmsgInfo) {
            return false;
        }

        $data = [
            "operator_id" => isset($data['operator_id']) ? $data['operator_id'] : 0,
            "source_id" => isset($data['source_id']) ? $data['source_id'] : 1,
            "is_jiechu" => isset($data['is_jiechu']) ? $data['is_jiechu'] : 0,
            "update_time" => time(),
            "sys" => $sys
        ];

        if ($data['is_jiechu']){
            $opInfo = '解除禁言';
        }else{
            $opInfo = '设为禁言';
        }

        $result = $this->model->updateByUserId($data, $userId, $sys);

        if ($result) {
            //is_jiechu=0禁言 1解除
            if ($data['is_jiechu'] == 0) {
                //添加redis禁言用户列表
                $key = $this->_getDisablesendmsgUserListKey($sys);
                Redis::getInstance()->redisZadd($key, time(), $userId);
            } else {
                //将用户删除redis禁言用户列表
                $key = $this->_getDisablesendmsgUserListKey($sys);
                Redis::getInstance()->redisZrem($key, $userId);
            }

            //添加操作日志
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_UPDATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $disablesendmsgInfo['id'], $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取用户的禁言列表的key
     */
    private function _getDisablesendmsgUserListKey($sys)
    {
        return "Disablesendmsg:{$sys}:users";
    }

    /**
     * 获取用户的禁言时长
     */
    public function getDisablesendmsgRemainingDaysByUserId($userId, $sys)
    {
        $disablesendmsgInfo = $this->model->getOneByUserId($userId, $sys);
        if (empty($disablesendmsgInfo)) {
            return false;
        }

        if (isset($disablesendmsgInfo['disablesendmsg_duration']) && $disablesendmsgInfo['is_jiechu'] == 0) {
            //禁言总天数
            $durationTime = $disablesendmsgInfo['disablesendmsg_duration'];
            if ($durationTime != 0) {
                //禁言时间
                $disablesendmsgTime = $disablesendmsgInfo['update_time'] ? $disablesendmsgInfo['update_time'] : $disablesendmsgInfo['disablesendmsg_time'];
                //禁言距今天数
                $spendDays = floor((time() - $disablesendmsgTime) / 86400) + 1;
                //禁言剩余天数
                $remainingDays = $durationTime - $spendDays + 1;
                $disablesendmsgRemainingDaysString = $remainingDays . '天';
                if ($remainingDays < 0) {
                    $disablesendmsgRemainingDaysString = '0天';
                }
            } else {
                $disablesendmsgRemainingDaysString = '永久';
            }
            return $disablesendmsgRemainingDaysString;
        } else {
            return NULL;
        }
    }

}