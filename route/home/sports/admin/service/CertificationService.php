<?php
/**
 * 实名认证
 */

namespace app\sports\admin\service;

use app\hook\adminLog\context\AdminLogContext;
use app\hook\adminLog\controller\AdminLog;
use app\sports\certification\model\CertificationModel;
use app\lucky\pay\service\UserGoldService;
use app\lucky\user\service\UserService;

class CertificationService
{
    //表名
    const ADMIN_OP_TABLE_NAME = 'certification';

    /*
     * 实名认证的列表
     */
    public function getList($data, $sys = 343)
    {
        $page = isset($data['page_no']) ? $data['page_no'] : 1;
        $pageCount = isset($data['page_count']) ? $data['page_count'] : 10;
        $auditFlag = isset($data['audit_flag']) ? $data['audit_flag'] : '';
        $nickname = isset($data['nickname']) ? $data['nickname'] : '';
        $phone = isset($data['phone']) ? $data['phone'] : '';
        if ($nickname && $phone) {
            $userIdArr = UserService::getInstance()->getUserInfoByLikeUserNicknameAndPhone($nickname, $phone, $sys);
        } elseif ($nickname) {
            $userIdArr = UserService::getInstance()->getUserInfoByLikeUserNickname($nickname, $sys);
        } elseif ($phone) {
            $userIdArr = UserService::getInstance()->getUserInfoByLikeUserPhone($phone, $sys);
        } else {
            $userIdArr = [0];
        }

        $result = CertificationModel::getInstance()->selectStatusListByPager($page, $pageCount, $auditFlag, $userIdArr, $sys);
        $data = $result['data'];

        //处理前端数据格式
        if ($data) {
            foreach ($data as $k => $v) {
                $userInfo = UserService::getInstance()->getUserInfo($v['user_id'], $sys);
                $data[$k]['nickname'] = isset($userInfo['nickname']) ? $userInfo['nickname'] : '';
                $data[$k]['phone'] = isset($userInfo['tel']) ? $userInfo['tel'] : '';
                $data[$k]['created_time'] = date('m-d H:i:s', $v['created_time']);
                switch ($v['audit_flag']) {
                    case 2:
                        $data[$k]['auditFlagName'] = '已通过';
                        break;
                    case 3:
                        $data[$k]['auditFlagName'] = '未通过';
                        break;
                    default:
                        $data[$k]['auditFlagName'] = '待审核';
                }
            }
        }

        $pageTotal = (int)ceil($result['total'] / $pageCount);
        $page_info = [
            'total' => $result['total'],//总记录
            'page_no' => $page,//页数
            'page_count' => $pageCount,//每页显示数量
            'page_total' => $pageTotal,//总页数
        ];

        return [
            'page_info' => $page_info,
            'list' => $data,
        ];
    }

    /**
     * 审核
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function audit($certificationId, $data, $sys = 343)
    {
        $oldCertificationInfo = CertificationModel::getInstance()->getOneById($certificationId, $sys);
        if (!$oldCertificationInfo) {
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "数据不存在", "data" => []];
        }

        $update = [
            'updated_time' => time(),
            'audit_flag' => $data['audit_flag'],
            "sys" => $sys
        ];
        if ($data['audit_flag'] == 2) {
            $opInfo = '审核通过实名认证信息';
        } else if ($data['audit_flag'] == 3) {
            $opInfo = '审核未通过实名认证信息';
        } else {
            $opInfo = '审核实名认证信息';
        }

        if (isset($data['audit_reason'])) {
            $update['audit_reason'] = $data['audit_reason'];
        }

        $result = CertificationModel::getInstance()->update($certificationId, $update, $sys);

        if ($result) {
            //添加操作日志
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_UPDATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $certificationId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            //实名认证成功调用的接口  目前,产品暂定,实名认证通过+50金豆，增加100经验值；一般只调用一次
            if (($data['audit_flag'] == 2) && ($oldCertificationInfo['audit_flag'] != 2)) {
                UserGoldService::getInstance()->authentication($oldCertificationInfo['user_id'], $sys);
            }

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "操作成功", "data" => []];
        } else {

            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "操作失败", "data" => []];
        }
    }

    /**
     * 信息
     * @return array 返回类型
     */
    public function getInfo($id, $sys = 343)
    {
        //实名认证信息
        $certificationInfo = CertificationModel::getInstance()->getOneById($id, $sys);
        if (!$certificationInfo) {
            return ["code" => _MSG_NOT_FOUND, "msg" => "查询失败", "data" => []];
        }
        //处理前端数据格式
        $userInfo = UserService::getInstance()->getUserInfo($certificationInfo['user_id'], $sys);
        $certificationInfo['nickname'] = isset($userInfo['nickname']) ? $userInfo['nickname'] : '';
        $certificationInfo['phone'] = isset($userInfo['tel']) ? $userInfo['tel'] : '';
        $certificationInfo['created_time'] = date('m-d H:i:s', $certificationInfo['created_time']);
        $certificationInfo['sex'] = $certificationInfo['sex'] == 1 ? '男' : '女';
        $certificationInfo['certificate_name'] = '身份证';
        switch ($certificationInfo['audit_flag']) {
            case 2:
                $certificationInfo['auditFlagName'] = '已通过';
                break;
            case 3:
                $certificationInfo['auditFlagName'] = '未通过';
                break;
            default:
                $certificationInfo['auditFlagName'] = '待审核';
        }

        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "查询成功", "data" => $certificationInfo];
    }

    /**
     * 信息
     * @return array 返回类型
     */
    public function delete($id, $sys = 343)
    {
        $result = certificationModel::getInstance()->delete($id, $sys);

        if ($result) {
            //添加操作日志
            $opInfo = '删除实名认证';
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_DELETE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $id, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);
        }

        return $result;
    }

}
