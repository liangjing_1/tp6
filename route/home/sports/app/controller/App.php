<?php


namespace app\sports\app\controller;

use app\sports\app\service\AppService;
use think\Exception;
use  app\sports\app\model\AppModel;
use app\common\BaseController;
use think\facade\Log;

class App extends BaseController
{
    protected $authCheck = false;
    protected $headerCheck = false;

    /*
     * 获取最新版本下载
     */
    public function getAppDownload()
    {
        try {
            $data = $this->request->post();
            $i_a_i = isset($data['i_a_i']) ? $data['i_a_i'] : 1;
            $appService = new AppService;
            $res = $appService->getAppDownloadService($i_a_i);
            if ($res) {
                $this->_success($res);
            } else {
                $this->_error(-1, '获取下载地址失败');
            }
        } catch (\Exception $e) {
            Log::error('app前端获取下载地址错误:' . $e->getMessage());
            $this->_error(-1, '获取下载地址错误!');
        }
    }
}