<?php
/**
 * 禁言列表参数格式化
 */
namespace app\sports\disablesendmsg\context;

use app\lucky\common\Context;

class DisablesendmsgListContext extends Context
{
    //页码
    public $page_no;
    //页数
    public $page_count;
    //操作者ID
    public $operatorId;
    //用户昵称
    public $user_nickname;
    //来源app1后台2
    public $sourceId;
    //是否删除1是0否
    public $isDel;
}