<?php
/**
 * 搜索
 */

namespace app\sports\search\validate;

use app\lucky\common\InstanceTrait;
use think\Validate;

class SearchValidate
{
    use InstanceTrait;

    public function checkSearch($data)
    {
        foreach ($data as $key => $value) {
            if (!is_array($value)) {
                $data[$key] = htmlentities($value);
            }
        }

        $rule = [
            "page_no" => "number|gt:0",
            "page_count" => "number|between:1,50",
            "search_name" => "require",
            "search_type" => "require|number|between:1,3"
        ];
        $msg = [
            "page_no.number" => "页码必须是数字",
            "page_no.gt" => "页码必须大于0",
            "page_count.number" => "每页数量必须是数字",
            "page_count.between" => "每页数量必须在1-50之间",
            "search_name.require" => "搜索内容必须填",
            "search_type.require" => "搜索类型必须填",
            "search_type.number" => "搜索类型必须是数字",
            "search_type.between" => "搜索类型必须在1-3之间"
        ];

        $validate = Validate::make($rule, $msg);
        $result = $validate->check($data);

        if (!$result) {
            return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => $validate->getError()];
        }
        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "校验成功", "data" => $data];
    }


}