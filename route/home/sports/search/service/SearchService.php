<?php
/**
 * 搜索
 */

namespace app\sports\search\service;


use app\lucky\admin\model\ExpertUserV2Model;
use app\lucky\follow\service\FollowService;
use app\lucky\user\model\UserV2Model;
use app\sports\match\service\FIFAService;

class SearchService
{
    public $model = '';

    /*
     * 查找获取所得列表
     */
    public function getSearchList($searchName, $type, $page = 1, $pageCount = 10, $sys = 343)
    {
        $result['total'] = 0;
        $data = [];

        if ($type == 1) {
            //掌球号列表
            $result = ExpertUserV2Model::getInstance()->listExpert($sys, $searchName, $page, $pageCount, 0);
            $result['total'] = $result['count'];
            //处理前端数据格式
            if ($result['list']) {
                foreach ($result['list'] as $k => $v) {
                    $data[] = [
                        'id' => $v['id'],
                        'name' => $v['name'],
                        'headimgurl' => $v['header'],
                        'desc' => $v['desc']
                    ];
                }
            }
        } elseif ($type == 2) {
            //用户
            $result = UserV2Model::getInstance()->selectStatusListByPager($page, $pageCount, [0], $sys, $searchName);
            //处理前端数据格式
            if ($result['data']) {
                $followService = new FollowService();
                foreach ($result['data'] as $k => $v) {
                    $data[] = [
                        'id' => $v['id'],
                        'name' => $v['nickname'],
                        'headimgurl' => $v['headimgurl'],
                        'followCount' => $followService->getUserFollowCount($v['id'], $sys)['count']
                    ];
                }
            }

        } elseif ($type == 3) {
            //赛事
            $FIFAService = new FIFAService();
            $eventsList = $FIFAService->getEventsList($page, $pageCount, -1, -1, $searchName);
            $result['data'] = isset($eventsList['data']['list']) ? $eventsList['data']['list'] : [];
            $result['total'] = isset($eventsList['data']['page_info']['total']) ? $eventsList['data']['page_info']['total'] : 0;

            //处理前端数据格式
            if ($result['data']) {
                foreach ($result['data'] as $k => $v) {
                    $data[] = [
                        'id' => $v['event_id'],
                        'name' => $v['name_zh'],
                        'logo' => $v['logo'],
                    ];
                }
            }
        }

        $pageTotal = (int)ceil($result['total'] / $pageCount);
        $page_info = [
            'total' => (int)$result['total'],//总记录
            'page_no' => (int)$page,//页数
            'page_count' => (int)$pageCount,//每页显示数量
            'page_total' => (int)$pageTotal,//总页数
        ];

        return [
            'page_info' => $page_info,
            'list' => $data,
        ];
    }

}