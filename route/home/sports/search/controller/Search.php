<?php
/**
 * 搜索
 */

namespace app\sports\search\controller;

use app\common\BaseController;
use app\sports\search\service\SearchService;
use app\sports\search\validate\SearchValidate;
use think\App;
use think\Request;

class Search extends BaseController
{
    protected $authCheck = false;
    protected $loginCheck = false;

    public $service = null;
    public $sys = '';

    public function __construct(App $app = null, Request $request = null)
    {
        parent::__construct($app, $request);
        $this->service = new SearchService();
        $this->sys = \session('sys') ? \session('sys') : 343;
    }

    /**
     * 查找获取所得列表
     * APP搜索页面 - 掌球号/用户/赛事 搜索
     * sports/search/getlist
     */
    public function getSearchList()
    {

        $postData = $this->request->post();
        $valiData = SearchValidate::getInstance()->checkSearch($postData);
        if ($valiData["code"] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($valiData["code"], $valiData["msg"]);
        }

        $postData = $valiData['data'];
//        类型1掌球号2用户3赛事
        $type = isset($postData['search_type']) ? $postData['search_type'] : 1;
        $page = isset($postData['page_no']) ? $postData['page_no'] : 1;
        $pageCount = isset($postData['page_count']) ? $postData['page_count'] : 10;
        $searchName = $postData['search_name'];

        $data = $this->service->getSearchList($searchName, $type, $page, $pageCount, $this->sys);

        $this->_jsonReturnV2(_MSG_SUCCESS, $data, '查询成功');
    }


}