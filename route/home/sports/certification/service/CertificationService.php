<?php
/**
 * 实名认证
 */

namespace app\sports\certification\service;

use app\sports\certification\model\CertificationModel;

class CertificationService
{
    //表名
    const ADMIN_OP_TABLE_NAME = 'certification';

    /**
     * 添加
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function create($data, $sys = 343)
    {
        $certificateInfo = CertificationModel::getInstance()->getOneByUserId($data['user_id'], $sys);
        if ($certificateInfo) {
//            数据存在就删除原来数据
            CertificationModel::getInstance()->deleteByUserId($data['user_id'], $sys);
        }

        $data = [
            "user_id" => isset($data['user_id']) ? $data['user_id'] : 0,
            "name" => $data['name'],
            "sex" => $data['sex'],
            "certificate_num" => $data['certificate_num'],
            "certificate_imgurl1" => isset($data['certificate_imgurl1']) ? $data['certificate_imgurl1'] : '',
            "certificate_imgurl2" => isset($data['certificate_imgurl2']) ? $data['certificate_imgurl2'] : '',
            "certificate_imgurl3" => isset($data['certificate_imgurl3']) ? $data['certificate_imgurl3'] : '',
            "created_time" => time(),
            "sys" => $sys
        ];

        $result = CertificationModel::getInstance()->insert($data);

        if ($result) {
            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "添加成功", "data" => []];
        } else {
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "添加失败", "data" => []];
        }
    }

    /**
     * 信息
     * @return array 返回类型
     */
    public function getInfo($userId, $sys = 343)
    {
        //实名认证信息
        $certificationInfo = CertificationModel::getInstance()->getOneByUserId($userId, $sys);
        if (!$certificationInfo) {
            return ["code" => _MSG_NOT_FOUND, "msg" => "查询失败", "data" => []];
        }

        //处理前端数据格式
        $certificationInfo['sex'] = $certificationInfo['sex'] == 1 ? '男' : '女';
        $certificationInfo['certificate_name'] = '身份证';
        $certificationInfo['certificate_imgstring'] = '已上传';
        $certificationInfo['name'] = substr_replace($certificationInfo['name'], '*', 0, 3);
        $certificationInfo['certificate_num'] = substr_replace($certificationInfo['certificate_num'], '****************', 1, 16);

        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "查询成功", "data" => $certificationInfo];
    }

    /**
     * 认证状态
     * @return array 返回类型
     */
    public function getAuditFlag($userId, $sys = 343)
    {
        //实名认证信息
        $certificationInfo = CertificationModel::getInstance()->getOneByUserId($userId, $sys);

        if (isset($certificationInfo['audit_flag'])) {
            switch ($certificationInfo['audit_flag']) {
                case 2:
                    $data['auditFlag'] = 2;
                    $data['auditFlagName'] = '已认证';
                    break;
                case 3:
                    $data['auditFlag'] = 3;
                    $data['auditFlagName'] = '未通过';
                    break;
                default:
                    $data['auditFlag'] = 1;
                    $data['auditFlagName'] = '审核中';
            }
        } else {
            $data['auditFlag'] = 0;
            $data['auditFlagName'] = '未认证';
        }

        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "查询成功", "data" => $data];
    }

    /**
     * 判断是否已认证
     * @return array 返回类型
     */
    public function isAudited($userId, $sys = 343)
    {
        //实名认证信息
        $certificationInfo = CertificationModel::getInstance()->getOneByUserId($userId, $sys);

        if (isset($certificationInfo['audit_flag'])) {
            if ($certificationInfo['audit_flag'] == 2) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
