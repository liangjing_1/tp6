<?php
/**
 * 实名认证
 */

namespace app\sports\certification\model;

use app\lucky\common\InstanceTrait;
use think\Db;

class CertificationModel
{
    use InstanceTrait;

    /**
     * 分页查询
     * @return array
     */
    public function selectStatusListByPager($page, $pageCount, $auditFlag, $userIdArr, $sys)
    {
        $op = Db::table('certification')
            ->where('sys', $sys)
            ->where('is_del', 0)
            ->field(array('id', 'user_id', 'created_time', 'certificate_num', 'audit_flag'))
            ->order('id desc');

        if (isset($auditFlag) && (!empty($auditFlag))) {
            $op = $op->where('audit_flag', $auditFlag);
        }
        if (isset($userIdArr) && ($userIdArr != [0])) {
            $op = $op->whereIn('user_id', $userIdArr);
        }

        //总共记录
        $count = count($op->select());
        $list = $op->page($page, $pageCount)->select();

        return [
            'data' => $list,
            'total' => $count
        ];
    }


    /**
     * @param $data
     * @description 添加实名认证
     */
    public function insert($data)
    {
        return Db::table("certification")->insert($data);
    }

    /**
     * @param $data
     * @description 修改信息
     */
    public function update($certificationId, $data, $sys)
    {
        return DB::table("certification")
            ->where("id", $certificationId)
            ->where("sys", $sys)
            ->update($data);
    }


    /**
     * 根据id获取一条信息
     * @param $id
     */
    public function getOneById($id, $sys)
    {
        return DB::table("certification")
            ->where("id", $id)
            ->where("sys", $sys)
            ->find();
    }

    /**
     * 根据userid获取一条信息
     * @param $userid
     */
    public function getOneByUserId($userId, $sys)
    {
        return DB::table("certification")
            ->where("user_id", $userId)
            ->where("sys", $sys)
            ->find();
    }

    /**
     * 获取实名认证id数组，根据实名认证名称模糊查找
     * @param $certificationName
     * @return certificationIdArr
     */
    public function getcertificationIdArrByLikecertificationName($certificationName, $sys)
    {
        $item = DB::table("certification")
            ->where("certification_name", 'like', "%{$certificationName}%")
            ->where("sys", $sys)
            ->field('id')
            ->select();

        $certificationIdArr = [];
        foreach ($item as $value) {
            $certificationIdArr[] = $value['id'];
        }

        return $certificationIdArr;
    }

    /**
     * 验证实名认证名字是否重复
     * @param $certificationId ,certification_name
     */
    public function checkcertificationnameRepetition($certificationId, $certificationName, $sys)
    {
        return DB::table("certification")
            ->where("id", "not in", $certificationId)
            ->where("certification_name", $certificationName)
            ->where("sys", $sys)
            ->find();
    }

    /**
     * 删除
     * @param $certification_id
     */
    public function delete($certificationId, $sys)
    {
        return DB::table("certification")
            ->where(["id" => $certificationId, 'sys' => $sys])
            ->update(["is_del" => 1, "deleted_time" => time()]);
    }

    /**
     * 删除
     * @param $certification_id
     */
    public function deleteByUserId($userId, $sys)
    {
        return DB::table("certification")
            ->where(["user_id" => $userId, 'sys' => $sys])
            ->delete();
    }

    /**
     * 通过ID获取实名认证列表
     */
    public function getListBycertificationIdArr($idArr, $sys)
    {
        $data = Db::table('certification')->whereIn('id', $idArr)
            ->where('sys', $sys)
            ->select();

        $certificationIdArr = [];
        foreach ($data as $value) {
            $certificationIdArr[$value['id']] = $value;
        }

        return $certificationIdArr;
    }

}
