<?php
/**
 * 实名认证
 */

namespace app\sports\certification\validate;

use app\lucky\common\InstanceTrait;
use think\Validate;

class CertificationValidate
{
    use InstanceTrait;

    public function checkAdd($data)
    {
        foreach ($data as $key => $value) {
            if (!is_array($value)) {
                $data[$key] = htmlentities($value);
            }
        }

        $rule = [
            "name" => "require|max:20",
            "certificate_num" => "require|min:15|max:18",
            "sex" => "require|number|between:1,2"
        ];
        $msg = [
            "name.require" => "姓名必填",
            "name.max" => "姓名不得超过10字",
            "certificate_num.require" => "证件号必填",
            "certificate_num.min" => "证件号必须在（15-18）之间",
            "certificate_num.max" => "证件号必须在（15-18）之间",
            "sex.require" => "性别必填",
            "sex.number" => "性别必须是数字",
            "sex.between" => "性别必须在（1-2）之间"
        ];

        $validate = Validate::make($rule, $msg);
        $result = $validate->check($data);

        if (!$result) {
            return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => $validate->getError()];
        }
        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "校验成功", "data" => $data];
    }

    public function checkUpdate($data)
    {
        foreach ($data as $key => $value) {
            if (!is_array($value)) {
                $data[$key] = htmlentities($value);
            }
        }

        $rule = [
            "id" => "require|number",
            "audit_reason" => "max:200",
            "audit_flag" => "require|number|between:2,3"
        ];
        $msg = [
            "id.require" => "实名认证ID必须提供",
            "id.number" => "实名认证ID必须是数字",
            "certification_name.max" => "实名认证名称不得超过100字",
            "audit_flag.require" => "审核结果必填",
            "audit_flag.number" => "审核结果必须是数字",
            "audit_flag.between" => "审核结果必须在（2-3）之间"
        ];

        $validate = Validate::make($rule, $msg);
        $result = $validate->check($data);

        if (!$result) {
            return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => $validate->getError()];
        }
        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "校验成功", "data" => $data];
    }

}
