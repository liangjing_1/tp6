<?php
/**
 * 实名认证
 */

namespace app\sports\certification\controller;

use app\sports\certification\service\CertificationService;
use app\common\BaseController;
use app\sports\certification\validate\CertificationValidate;
use think\App;
use think\Request;


class Certification extends BaseController
{
    public $service = null;
    public $sys = '';

    public function __construct(App $app = null, Request $request = null)
    {
        parent::__construct($app, $request);
        $this->service = new CertificationService();
        $this->sys = \session('sys') ? \session('sys') : 343;
    }

    /**
     * @description 实名认证 认证状态
     * 路由：可访问 /sports/certification/info
     */
    public function getAuditFlag()
    {
        $result = $this->service->getAuditFlag($this->loginInfo['user_id'], $this->sys);

        if ($result['code']!=_MSG_SYSTEM_SUCCESS){
            $this->_error($result['code'], $result['msg']);
        }

        $this->_success($result["data"], $result["msg"]);
    }

    /**
     * @description 实名认证 详情
     * 路由：可访问 /sports/certification/info
     */
    public function getInfo()
    {
        $result = $this->service->getInfo($this->loginInfo['user_id'], $this->sys);

        if ($result['code']!=_MSG_SYSTEM_SUCCESS){
            $this->_error($result['code'], $result['msg']);
        }
        $this->_success($result["data"], $result["msg"]);
    }

    /**
     * @description 实名认证 发布
     * 路由：可访问 /admin/certification/create
     */
    public function create()
    {
        $postData = $this->request->post();
        $valiData = CertificationValidate::getInstance()->checkAdd($postData);
        if ($valiData["code"] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($valiData["code"], $valiData["msg"]);
        }
        $postData = $valiData['data'];
        $postData['user_id'] = $this->loginInfo['user_id'];
        $result = $this->service->create($postData, $this->sys);

        if ($result['code'] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($result['code'], $result["msg"]);
        }

        $this->_success([], $result["msg"]);
    }

}
