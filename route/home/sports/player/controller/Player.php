<?php
/**
 *
 */

namespace app\sports\player\controller;


use app\common\BaseController;
use app\sports\player\service\PlayerService;

class Player extends BaseController
{
    private $database = 'sports_database';
    protected $authCheck = false;
    protected $headerCheck = false;

    /**
     * 球员基本信息
     * sports/player/detail
     * */
    public function getPlayerDetail()
    {
        $data = $this->request->post();
        $namiPlayerId = isset($data['nami_player_id']) ? $data['nami_player_id'] : 0;
        if (empty($namiPlayerId) || !isset($namiPlayerId)) {
            $this->_jsonReturnV2(-1, [], '请选择一球员');
        }

        $playerService = new PlayerService();
        $data = $playerService->getPlayerDetail($namiPlayerId);

        $this->_jsonReturn($data);
    }

    /**
     * 球员基本信息
     * sports/player/stroke_analysis
     * */
    public function getPlayerStrokeAnalysis()
    {
        $data = $this->request->post();
        $namiPlayerId = isset($data['nami_player_id']) ? $data['nami_player_id'] : 0;
        if (empty($namiPlayerId) || !isset($namiPlayerId)) {
            $this->_jsonReturnV2(-1, [], '请选择一球员');
        }
        $seasonId = isset($data['season_id']) ? $data['season_id'] : 0;
        if (empty($seasonId) || !isset($seasonId)) {
            $this->_jsonReturnV2(-1, [], '请选择一赛季');
        }

        $playerService = new PlayerService();
        $data = $playerService->getPlayerStrokeAnalysis($namiPlayerId, $seasonId);

        $this->_jsonReturn($data);
    }

    /**
     * 球员参加比赛列表
     * sports/player/eventlist
     * */
    public function getEventList()
    {
        $data = $this->request->post();
        $namiPlayerId = isset($data['nami_player_id']) ? $data['nami_player_id'] : 0;
        if (empty($namiPlayerId) || !isset($namiPlayerId)) {
            $this->_jsonReturnV2(-1, [], '请选择一球员');
        }

        $playerService = new PlayerService();
        $data = $playerService->getEventList($namiPlayerId);

        $this->_jsonReturn($data);
    }


}