<?php
/**
 * 球员服务
 */

namespace app\sports\player\service;

use app\common\Http;

class PlayerService
{
    private $url = '';

    public function __construct()
    {
        if (env('APP_ENV') == 'local') {
            $this->url = 'http://kaijiang.web.com/index.php/';
        } elseif (env('APP_ENV') == 'test') {
            $this->url = 'https://sports-api-dev.343.cn/';
        } else if (env('APP_ENV') == 'prod') {
            $this->url = 'https://sports-api.242.cn/';
        }
    }

    /**
     * 球员基本信息
     */
    public function getPlayerDetail($namiPlayerId)
    {
        try {
            $uri = 'sports/player/detail';
            $data = Http::post($this->url . $uri, [], ['nami_player_id' => $namiPlayerId]);
            $data = json_decode($data->body, true);

            if (!isset($data['data'])) {
                throw new \Exception('服务接口出现错误 -1');
            }

            return $data;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return ['code' => -1, 'msg' => $e->getMessage()];
        }
    }

    /**
     * 球员技术统计
     */
    public function getPlayerStrokeAnalysis($namiPlayerId, $seasonId)
    {
        try {
            $uri = 'sports/player/stroke_analysis';
            $data = Http::post($this->url . $uri, [], ['nami_player_id' => $namiPlayerId, 'season_id' => $seasonId]);
            $data = json_decode($data->body, true);

            if (!isset($data['data'])) {
                throw new \Exception('服务接口出现错误 -1');
            }

            return $data;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return ['code' => -1, 'msg' => $e->getMessage()];
        }
    }

    /**
     * 球员参加比赛列表
     */
    public function getEventList($namiPlayerId)
    {
        try {
            $uri = 'sports/player/eventlist';
            $data = Http::post($this->url . $uri, [], ['nami_player_id' => $namiPlayerId]);
            $data = json_decode($data->body, true);

            if (!isset($data['data'])) {
                throw new \Exception('服务接口出现错误 -1');
            }

            return $data;
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return ['code' => -1, 'msg' => $e->getMessage()];
        }
    }

}