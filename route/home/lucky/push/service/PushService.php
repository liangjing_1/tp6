<?php
/**
 * 推送服务
 */

namespace app\lucky\push\service;

use app\common\JPush;
use app\lucky\follow\service\FollowService;
use app\lucky\push\model\UserPushConfigModel;
use app\lucky\subscribe\service\SubscribeService;
use app\sports\match\service\FollowMatchService;
use app\sports\match\service\SportsApiService;

class PushService
{
    public function push()
    {
        try {
            $push = JPush::getInstance()->push(['1517bfd3f752006e81e'], '我是标题', '我是内容', 'GameDetails:1909991');
            var_dump($push);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

    }

    /**
     * 推送设置
     */
    public function pushConfig($userId, $sys, $ext)
    {
        $userPushConfigModel = new UserPushConfigModel();
        $result = $userPushConfigModel->updateConfig($userId, $sys, ['ext' => json_encode($ext), 'update_time' => time()]);

        if ($result) {
            return ['code' => 0, 'msg' => '添加成功'];
        }

        return ['code' => -1, 'msg' => '添加失败'];
    }

    /**
     * 获取配置
     */
    public function getPushConfig($userId, $sys)
    {
        $userPushConfigModel = new UserPushConfigModel();
        $result = $userPushConfigModel->getConfigByUserId($userId, $sys);
        $data = isset($result['ext']) ? $result['ext'] : '';
        $data = (array)json_decode($data, true);

        return ['code' => 0, 'msg' => '获取成功', 'data' => $data];
    }

    /**
     * 比赛相关信息推送
     */
    public function matchPush($data)
    {
        //获取关注比赛的用户ID
        $matchId = array_column($data, 'match_id');
        $followMatchService = new FollowMatchService();
        $followData = $followMatchService->getFollowUserByMatchIdArr($matchId);
        if (empty($followData)) {
            return ['code' => -1, 'msg' => '未有用户关注比赛'];
        }

        //获取用户ID和极光推送ID的关联
        $userId = [];
        foreach ($followData as $key => $value) {
            foreach ($value as $k => $v) {
                $userId[] = $v['user_id'];
            }
        }
        $userId = array_unique($userId);
        $pushModel = new UserPushConfigModel();
        $pushConfig = $pushModel->getConfigByUserIdArr($userId, 'sports');

        //根据比赛ID获取比赛比分
        $sportsApiService = new SportsApiService();

        //根据比赛ID获取比赛数据
        $matchInfo = $sportsApiService->getMatchInfoByMatchIdArr($matchId);
        $matchInfo = isset($matchInfo['data']) ? $matchInfo['data'] : [];

        //按比赛ID分别推送不同的用户信息
        $scoreInfo = $sportsApiService->getScoreInfoByMatchId($matchId);
        $scoreInfo = isset($scoreInfo['data']) ? $scoreInfo['data'] : [];

        //获取用户和需要通知的类型之间的关联
        $matchUserType = [];
        foreach ($followData as $key => $value) {
            //$key 比赛ID $value array 关注该比赛的用户ID
            foreach ($value as $k => $v) {
                if (!isset($pushConfig[$v['user_id']])) {
                    continue;
                }

                $ext = (array)json_decode($pushConfig[$v['user_id']]['ext'], true);

                if (in_array('start', $ext)) {
                    //开始
                    $matchUserType[$key]['begin'][] = $v['user_id'];
                }

                if (in_array('end', $ext)) {
                    //结束
                    $matchUserType[$key]['end'][] = $v['user_id'];
                }

                if (in_array('score', $ext)) {
                    //进球
                    $matchUserType[$key]['enter'][] = $v['user_id'];
                }
            }
        }

        //比赛ID 比赛类型 需要推送的用户标识
        foreach ($data as $value) {
            if ($value['type'] == 10) {
                //开始 获取该场比赛该类型的用户ID集合
                if (isset($matchUserType[$value['match_id']]) && isset($matchUserType[$value['match_id']]['begin'])) {
                    $matchId = $value['match_id'];
                    $userIdArr = $matchUserType[$matchId]['begin'];
                    $identifyArr = [];
                    foreach ($userIdArr as $userId) {
                        if (!isset($pushConfig[$userId])) {
                            continue;
                        }

                        $identifyArr[] = $pushConfig[$userId]['identify'];
                    }

                    if (!isset($matchInfo[$matchId])) {
                        continue;
                    }

                    $matchRes = $matchInfo[$matchId];
                    //开场 只有开场的那一分钟推送 由于开始比较特殊 数据会在开始前一直传过来 做个处理
                    if (time() - $matchRes['begin'] > 30 || $matchRes['begin'] - time() > 30) {
                        continue;
                    }

                    $leftTeamName = $matchRes['left_team_name'];
                    $rightTeamName = $matchRes['right_team_name'];

                    $title = "{$leftTeamName} VS {$rightTeamName}";
                    $text = "比赛即将开始, 点击查看比赛详情";
                    try {
                        JPush::getInstance()->push($identifyArr, $title, $text, 'GameDetails', $matchId);
                    } catch (\Exception $e) {
                        \Log::error($e->getMessage());
                    }
                }
            }

            if ($value['type'] == 12) {
                //结束
                if (isset($matchUserType[$value['match_id']]) && isset($matchUserType[$value['match_id']]['end'])) {
                    $userIdArr = $matchUserType[$value['match_id']]['end'];
                    $identifyArr = [];
                    foreach ($userIdArr as $userId) {
                        if (!isset($pushConfig[$userId])) {
                            continue;
                        }

                        $identifyArr[] = $pushConfig[$userId]['identify'];
                    }

                    if (!isset($matchInfo[$matchId]) || !isset($scoreInfo[$matchId])) {
                        continue;
                    }

                    $matchRes = $matchInfo[$matchId];
                    $scoreRes = $scoreInfo[$matchId];

                    $leftTeamName = $matchRes['left_team_name'];
                    $rightTeamName = $matchRes['right_team_name'];

                    $leftTeamScore = $scoreRes['left_bifen'];
                    $rightTeamScore = $scoreRes['right_bifen'];

                    $title = "{$leftTeamName} VS {$rightTeamName}";
                    $text = "比赛已结束，两队比分{$leftTeamScore}:{$rightTeamScore} 点击查看更多数据";
                    try {
                        JPush::getInstance()->push($identifyArr, $title, $text, 'GameDetails', $matchId);
                    } catch (\Exception $e) {
                        \Log::error($e->getMessage());
                    }
                }
            }

            if ($value['type'] == 1) {
                //进球
                if (isset($matchUserType[$value['match_id']]) && isset($matchUserType[$value['match_id']]['enter'])) {
                    $userIdArr = $matchUserType[$value['match_id']]['enter'];
                    $identifyArr = [];
                    foreach ($userIdArr as $userId) {
                        if (!isset($pushConfig[$userId])) {
                            continue;
                        }

                        $identifyArr[] = $pushConfig[$userId]['identify'];
                    }

                    if (!isset($matchInfo[$matchId]) || !isset($scoreInfo[$matchId])) {
                        continue;
                    }

                    $matchRes = $matchInfo[$matchId];
                    $scoreRes = $scoreInfo[$matchId];

                    $leftTeamName = $matchRes['left_team_name'];
                    $rightTeamName = $matchRes['right_team_name'];

                    $leftTeamScore = $scoreRes['left_bifen'];
                    $rightTeamScore = $scoreRes['right_bifen'];

                    $enterTeam = $value['position'] == 1 ? $leftTeamName : $rightTeamName;
                    $enterScore = $value['position'] == 1 ? $leftTeamScore : $rightTeamScore;

                    $title = "{$leftTeamName} VS {$rightTeamName}";
                    $text = "比赛正在进行中, {$enterTeam}进了本场第{$enterScore}个进球 点击查看更多数据";
                    try {
                        JPush::getInstance()->push($identifyArr, $title, $text, 'GameDetails', $matchId);
                    } catch (\Exception $e) {
                        \Log::error($e->getMessage());
                    }
                }
            }
        }
    }

    /**
     * 发布资讯推送
     */
    public function blogPush($authorId, $title, $text, $blogId)
    {
        //获取作者的粉丝列表ID
        $followService = new FollowService();
        $followListId = $followService->getAuthorFollowList($authorId, 'sports');

        //获取用户ID的配置
        $pushModel = new UserPushConfigModel();
        $pushConfig = $pushModel->getConfigByUserIdArr($followListId, 'sports');

        $identifyArr = [];
        foreach ($pushConfig as $value) {
            $ext = (array)json_decode($value['ext'], true);
            if (in_array('information', $ext)) {
                $identifyArr[] = $value['identify'];
            }
        }

        if (!empty($identifyArr)) {
            try {
                JPush::getInstance()->push($identifyArr, $title, $text, 'InfoDetails', $blogId);
            } catch (\Exception $exception) {
                \Log::error($exception->getMessage());
            }
        }
    }

    /**
     * 发布推单推送
     */
    public function sheetPush($expertUserId, $title, $text, $sheetId)
    {
        //获取专家号的粉丝列表ID
        $subscribeService = new SubscribeService();
        $followListId = $subscribeService->getUserByExpertUserId($expertUserId, 'sports');
        $followListId = array_column($followListId, 'user_id');

        //获取用户ID的配置
        $pushModel = new UserPushConfigModel();
        $pushConfig = $pushModel->getConfigByUserIdArr($followListId, 'sports');

        $identifyArr = [];
        foreach ($pushConfig as $value) {
            $ext = (array)json_decode($value['ext'], true);
            if (in_array('news', $ext)) {
                $identifyArr[] = $value['identify'];
            }
        }

        if (!empty($identifyArr)) {
            try {
                JPush::getInstance()->push($identifyArr, $title, $text, 'RecommendDetails', $sheetId);
            } catch (\Exception $exception) {
                \Log::error($exception->getMessage());
            }
        }
    }

    /**
     * 登录关联极光ID
     */
    public function loginLinkPush($userId, $identify, $sys = '343')
    {
        $userPushConfigModel = new UserPushConfigModel();
        $config = $userPushConfigModel->getConfigByUserId($userId, 'sports');

        if (empty($config)) {
            $data = [
                'user_id' => $userId,
                'identify' => $identify,
                'update_time' => time(),
                'sys' => $sys,
                'ext' => json_encode(['start' => true, 'end' => true, 'score' => true, 'news' => true, 'information' => true])
            ];

            $result = $userPushConfigModel->addConfig($data);
            if (empty($result)) {
                return ['code' => -1, 'msg' => '添加极光推送失败'];
            }

            return ['code' => 0, 'msg' => '添加极光推送成功'];
        }

        $data = [
            'identify' => $identify,
            'update_time' => time(),
        ];
        $result = $userPushConfigModel->updateConfig($userId, $sys, $data);
        if (empty($result)) {
            return ['code' => -1, 'msg' => '更新极光推送失败'];
        }

        return ['code' => 0, 'msg' => '更新极光推送成功'];
    }

    /**
     * 退出登录关联极光ID
     */
    public function logoutLinkPush($userId, $sys = '343')
    {
        $userPushConfigModel = new UserPushConfigModel();
        $data = [
            'identify' => '',
            'update_time' => time(),
        ];
        $result = $userPushConfigModel->updateConfig($userId, $sys, $data);
        if (empty($result)) {
            return ['code' => -1, 'msg' => '退出登录,更新极光推送失败'];
        }

        return ['code' => 0, 'msg' => '退出登录,更新极光推送成功'];
    }
}