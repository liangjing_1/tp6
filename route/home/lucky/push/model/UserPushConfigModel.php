<?php
/**
 * 极光推送配置
 */
namespace app\lucky\push\model;

use think\Db;
use think\Model;

class UserPushConfigModel extends Model
{
    /**
     * 获取配置
     */
    public function getConfigByUserId($userId, $sys)
    {
        $return = $this->table('user_push_config')->where(['user_id' => $userId, 'sys' => $sys])->find();
        return $return;
    }

    /**
     * 新增配置
     */
    public function addConfig($data)
    {
        $result = $this->table('user_push_config')->insert($data);
        return $result;
    }

    /**
     * 更新配置
     */
    public function updateConfig($userId, $sys, $data)
    {
        $result = $this->table('user_push_config')->where(['user_id' => $userId, 'sys' => $sys])->update($data);
        return $result;
    }

    /**
     * 根据用户ID获取配置
     */
    public function getConfigByUserIdArr($userId, $sys)
    {
        $data = Db::table('user_push_config')->whereIn('user_id', $userId)->where(['sys' => $sys])
            ->select();

        $return = [];
        foreach ($data as $value) {
            $return[$value['user_id']] = $value;
        }

        return $return;
    }
}