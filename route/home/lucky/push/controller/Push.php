<?php
/**
 * 推送设置
 */
namespace app\lucky\push\controller;

use app\common\BaseController;
use app\lucky\push\service\PushService;
use think\Controller;

class Push extends BaseController
{
    protected $authCheck = false;
    /**
     * 推送配置的开关
     */
    public function pushConfig()
    {
        $data = $this->request->post();
        $ext = isset($data['ext']) ? $data['ext'] : [];
        $sys = session('sys');

        $userId = $this->checkLoginV2();
        $pushService = new PushService();
        $return = $pushService->pushConfig($userId, $sys, $ext);

        $this->_jsonReturn($return);
    }

    /**
     * 获取配置
     */
    public function getPushConfig()
    {
        $sys = session('sys');

        $userId = $this->checkLoginV2();
        $pushService = new PushService();
        $return = $pushService->getPushConfig($userId, $sys);

        $this->_jsonReturn($return);
    }

    /**
     * 足球比赛事件数据的推送
     */
    public function matchPush()
    {
        $list = $this->request->post();

        $pushService = new PushService();
        $pushService->matchPush($list);
    }
}