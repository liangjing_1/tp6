<?php
/**
 * 频道
 */

namespace app\lucky\channel\model;

use app\lucky\common\InstanceTrait;
use think\Db;

class ChannelModel
{
    use InstanceTrait;

    const CHANNEL_STATUS_NORMAL = 1; //正常
    const CHANNEL_STATUS_APP_DELETE = 2; //app删除
    const CHANNEL_STATUS_ADMIN_DELETE = 3; //后台删除

    /**
     * 分页查询
     * @return array
     */
    public function selectStatusListByPager($page, $pageCount, $search, $sys)
    {
        $op = Db::table('channel')
            ->where('status', self::CHANNEL_STATUS_NORMAL)
            ->where('sys', $sys)
            ->field(array('id', 'name', 'create_time', 'is_show'))
            ->order('id desc');

        if (isset($search) && (!empty($search))) {
            $op = $op->whereLike('name', "%{$search}%");
        }

        //总共记录
        $count = count($op->select());
        $list = $op->page($page, $pageCount)->select();

        return [
            'data' => $list,
            'total' => $count
        ];
    }

    /**
     * @param $data
     * @description 添加频道
     */
    public function insert($data)
    {
        return Db::table("channel")->insertGetId($data);
    }

    /**
     * @param $data
     * @description 修改信息
     */
    public function update($channelId, $data, $sys)
    {
        return DB::table("channel")
            ->where("id", $channelId)
            ->where("sys", $sys)
            ->update($data);
    }


    /**
     * 根据id获取一条信息
     * @param $id
     */
    public function getOneById($id, $sys, $isShow = '')
    {
        $op = DB::table("channel")
            ->where("id", $id)
            ->where('status', self::CHANNEL_STATUS_NORMAL)
            ->where("sys", $sys);

        if ($isShow != '') {
            $op = $op->where("is_show", $isShow);
        }

        return $op->find();
    }

    public function getOneByName($name, $sys)
    {
        return DB::table("channel")
            ->where('status', self::CHANNEL_STATUS_NORMAL)
            ->where("name", $name)
            ->where("sys", $sys)
            ->find();
    }

    public function getOneByNameAndNotInById($id, $name, $sys)
    {
        return DB::table("channel")
            ->where('status', self::CHANNEL_STATUS_NORMAL)
            ->whereNotIn("id", $id)
            ->where("name", $name)
            ->where("sys", $sys)
            ->find();
    }

    /**
     * 获取频道id数组，根据频道名称模糊查找
     * @param $channelName
     * @return channelIdArr
     */
    public function getchannelIdArrByLikechannelName($channelName, $sys)
    {
        $item = DB::table("channel")
            ->where('status', self::CHANNEL_STATUS_NORMAL)
            ->where("channel_name", 'like', "%{$channelName}%")
            ->where("sys", $sys)
            ->field('id')
            ->select();

        $channelIdArr = [];
        foreach ($item as $value) {
            $channelIdArr[] = $value['id'];
        }

        return $channelIdArr;
    }

    /**
     * 验证频道名字是否重复
     * @param $channelId ,channel_name
     */
    public function checkchannelnameRepetition($channelId, $channelName, $sys)
    {
        return DB::table("channel")
            ->where('status', self::CHANNEL_STATUS_NORMAL)
            ->where("id", "not in", $channelId)
            ->where("channel_name", $channelName)
            ->where("sys", $sys)
            ->find();
    }

    /**
     * 删除
     * @param $channel_id
     */
    public function delete($channel_id, $sys)
    {
        return DB::table("channel")
            ->where(["id" => $channel_id, 'sys' => $sys])
            ->update(["status" => self::CHANNEL_STATUS_ADMIN_DELETE, "delete_time" => time()]);
    }


    /**
     * 通过ID获取频道列表
     */
    public function getListBychannelIdArr($idArr, $sys)
    {
        $data = Db::table('channel')
            ->where('status', self::CHANNEL_STATUS_NORMAL)
            ->whereIn('id', $idArr)
            ->where('sys', $sys)
            ->select();

        $channelIdArr = [];
        foreach ($data as $value) {
            $channelIdArr[$value['id']] = $value;
        }

        return $channelIdArr;
    }


    /**
     * 获取频道id数组，根据频道名称模糊查找
     * @param $channelName
     * @return channelIdArr
     */
    public function getChannelAllList($sys, $isShow = '')
    {
        $op = DB::table("channel")
            ->where('status', self::CHANNEL_STATUS_NORMAL)
            ->where("sys", $sys)
            ->field('id,name');

        if ($isShow != '') {
            $op = $op->where("is_show", $isShow);
        }

        return $op->select();
    }

}