<?php
/**
 * 频道
 */

namespace app\lucky\channel\service;

use app\common\Redis;
use app\lucky\channel\model\ChannelModel;

class ChannelService
{

    /*
     * 用户的频道列表
     */
    public function getUserList($userId, $sys = 343)
    {
        $key = $sys . ':channel:user:' . $userId;
        $result = Redis::getInstance()->redisGet($key);
        $data['list'] = [];

        if (!$result['data']) {
            $data['list'] = [];
        } else {
            $chnnelIdArr = explode(',', $result['data']);
            foreach ($chnnelIdArr as $k => $v) {
                $chnnelInfo = ChannelModel::getInstance()->getOneById($v, $sys, 1);

                if (isset($chnnelInfo['id']) && isset($chnnelInfo['name'])) {
                    $chnnelArr['id'] = $chnnelInfo['id'];
                    $chnnelArr['name'] = $chnnelInfo['name'];
                    $data['list'][] = $chnnelArr;
                }
            }
        }

        return $data;
    }

    /*
     * 频道的所有列表
     */
    public function getList($userId, $sys = 343)
    {
        $data['allList'] = [];
        $data['userChannelList'] = [];

        //用户频道列表
        $result = $this->getUserList($userId, $sys);
        $data['userChannelList'] = $result['list'];

        $result = $this->getAllList($sys);
        $data['allList'] = $result['list'];
//        删除用户频道数据
        if ($data['allList'] && $data['userChannelList']) {
            foreach ($data['allList'] as $k => $v) {
                foreach ($data['userChannelList'] as $uk => $uv) {
                    if ($v['name'] == $uv['name']) {
                        unset($data['allList'][$k]);
                    }
                }
            }
        }
        sort($data['allList']);

        return $data;
    }

    /*
     * 频道的所有列表
     */
    public function getAllList($sys = 343)
    {
        $data['list'] = ChannelModel::getInstance()->getChannelAllList($sys, 1);
        if (!$data) {
            $data['list'] = [];
        }

        return $data;
    }


    /**
     * 修改用户频道
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function editUserChannel($channelIdStrs, $userId, $sys = 343)
    {
        $key = $sys . ':channel:user:' . $userId;

        if ($channelIdStrs == 0) {
            //用户取消所有的订阅频道
            //删除key
            Redis::getInstance()->redisDel($key);
        } else {
            Redis::getInstance()->redisSet($key, $channelIdStrs);
        }

        return ["code" => 0, "msg" => "操作成功", "data" => []];
    }

}