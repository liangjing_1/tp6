<?php
/**
 * 频道
 */

namespace app\lucky\channel\controller;

use app\lucky\channel\service\ChannelService;
use app\common\BaseController;
use app\lucky\channel\validate\ChannelValidate;
use think\App;
use think\Request;


class Channel extends BaseController
{
    public $service = null;
    public $sys = '';

    public function __construct(App $app = null, Request $request = null)
    {
        parent::__construct($app, $request);
        $this->service = new ChannelService();
        $this->sys = \session('sys') ? \session('sys') : 343;
    }

    /**
     * @description  获取用户频道列表
     * 路由：可访问 /sports/channel/user_list
     */
    public function getUserList()
    {
        $userId = $this->loginInfo['user_id'];

        $data = $this->service->getUserList($userId, $this->sys);

        $this->_jsonReturnV2(0, $data, 'success');
    }

    /**
     * @description  获取所有列表
     * 路由：可访问 /sports/channel/list
     */
    public function getList()
    {
        $userId = $this->loginInfo['user_id'];
        $result = $this->service->getList($userId, $this->sys);

        $this->_jsonReturnV2(0, $result, 'success');
    }

    /**
     * @description 修改用户频道
     * 路由：可访问 sports/channel/edit_user
     */
    public function editUserChannel()
    {
        $postData = $this->request->post();
        $valiData = ChannelValidate::getInstance()->checkEditUserChannel($postData);
        if ($valiData["code"] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($valiData["code"], $valiData["msg"]);
        }

        $postData = $valiData['data'];
        $userId = $this->loginInfo['user_id'];

        $result = $this->service->editUserChannel($postData['channel_id_strs'], $userId, $this->sys);

        $this->_jsonReturnV2($result['code'], [], $result['msg']);
    }


}