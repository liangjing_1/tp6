<?php
/**
 * 频道
 */

namespace app\lucky\channel\validate;

use app\lucky\common\InstanceTrait;
use think\Validate;

class ChannelValidate
{
    use InstanceTrait;

    public function checkAdd($data)
    {
        foreach ($data as $key => $value) {
            if (!is_array($value)) {
                $data[$key] = htmlentities($value);
            }
        }

        $rule = [
            "name" => "require|max:10",
        ];
        $msg = [
            "name.require" => "频道名称必填",
            "name.max" => "频道名称不得超过10字符",
        ];

        $validate = Validate::make($rule, $msg);
        $result = $validate->check($data);

        if (!$result) {
            return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => $validate->getError()];
        }
        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "校验成功", "data" => $data];
    }

    public function checkUpdate($data)
    {
        foreach ($data as $key => $value) {
            if (!is_array($value)) {
                $data[$key] = htmlentities($value);
            }
        }

        $rule = [
            "channel_id" => "require|number",
            "name" => "require|max:10",
        ];
        $msg = [
            "channel_id.require" => "频道ID必须提供",
            "channel_id.number" => "频道ID必须是数字",
            "name.require" => "频道名称必须提供",
            "name.max" => "频道名称不得超过10字符",
        ];

        $validate = Validate::make($rule, $msg);
        $result = $validate->check($data);

        if (!$result) {
            return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => $validate->getError()];
        }
        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "校验成功", "data" => $data];
    }

    public function checkEditUserChannel($data)
    {
        foreach ($data as $key => $value) {
            if (!is_array($value)) {
                $data[$key] = htmlentities($value);
            }
        }

        $rule = [
            "channel_id_strs" => "require"
        ];
        $msg = [
            "channel_id_strs.require" => "频道Ids必须提供"
        ];

        $validate = Validate::make($rule, $msg);
        $result = $validate->check($data);

        if (!$result) {
            return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => $validate->getError()];
        }
        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "校验成功", "data" => $data];
    }

    public function updateIsShow($data)
    {
        foreach ($data as $key => $value) {
            if (!is_array($value)) {
                $data[$key] = htmlentities($value);
            }
        }

        $rule = [
            "channel_id" => "require|number",
            "is_show" => "require|between:0,1",
        ];
        $msg = [
            "channel_id.require" => "频道ID必须提供",
            "channel_id.number" => "频道ID必须是数字",
            "is_show.require" => "是否开启必须提供",
            "is_show.between" => "是否开启必须在0-1之间",
        ];

        $validate = Validate::make($rule, $msg);
        $result = $validate->check($data);

        if (!$result) {
            return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => $validate->getError()];
        }
        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "校验成功", "data" => $data];
    }

}