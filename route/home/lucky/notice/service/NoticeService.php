<?php
/**
 * 公告　
 */

namespace app\lucky\notice\service;

use app\lucky\notice\model\NoticeModel;

class NoticeService
{
    public $model = '';

    public function __construct()
    {
        $this->model = new NoticeModel();
    }


    /**
     * 查询列表
     * @access public
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function getList($data, $sys = 343)
    {
        $page = isset($data['page_no']) ? $data['page_no'] : 1;
        $pageCount = isset($data['page_count']) ? $data['page_count'] : 10;
        $search = isset($data['search']) ? $data['search'] : '';

        $result = $this->model->selectStatusListByPager($page, $pageCount, $search, $sys);

        $data = $result['data'];
        //处理前端数据格式
        if ($data) {
            foreach ($data as $k => $v) {
                if ($v['updated_at']) {
                    $data[$k]['created_time'] = date('m-d H:i', $v['updated_at']);
                } else {
                    $data[$k]['created_time'] = date('m-d H:i', $v['created_at']);
                }

            }
        }

        $pageTotal = (int)ceil($result['total'] / $pageCount);
        $page_info = [
            'total' => $result['total'],//总记录
            'page_no' => $page,//页数
            'page_count' => $pageCount,//每页显示数量
            'page_total' => $pageTotal,//总页数
        ];

        return [
            'page_info' => $page_info,
            'list' => $data,
        ];
    }

}