<?php
/**
 * banner
 */

namespace app\lucky\banner\service;

use app\lucky\banner\model\BannerModel;

class BannerService
{
    /**
     * 信息
     * @return array 返回类型
     */
    public function getInfo($bannerId, $sys = 343)
    {
        $bannerInfo = BannerModel::getInstance()->getOneById($bannerId, $sys);

        if (!$bannerInfo) {
            return ["code" => _MSG_NOT_FOUND, "msg" => "查询失败", "data" => []];
        }

        //处理前端数据格式
        $bannerInfo['content'] = htmlspecialchars_decode($bannerInfo['content']);

        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "查询成功", "data" => $bannerInfo];
    }

}