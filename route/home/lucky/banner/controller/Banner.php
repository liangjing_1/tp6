<?php
/**
 * banner
 */

namespace app\lucky\banner\controller;

use app\lucky\banner\service\BannerService;
use app\common\BaseController;
use think\App;
use think\Request;


class Banner extends BaseController
{
    protected $authCheck = false;
    protected $loginCheck = false;
    public $service = null;
    public $sys = '';

    public function __construct(App $app = null, Request $request = null)
    {
        parent::__construct($app, $request);
        $this->service = new bannerService();
        $this->sys = \session('sys') ? \session('sys') : 343;
    }

    /**
     * @description banner 详情
     * 路由：可访问 /sports/banner/info
     */
    public function getInfo()
    {
        $post = $this->request->post();
        if (!isset($post["id"])) {
            $this->_jsonReturnV2(_MSG_INVALID_CLIENT_PARAM, '', '必须提供id');
        }

        $result = $this->service->getInfo($post["id"], $this->sys);

        $this->_jsonReturnV2($result['code'], $result['data'], $result['msg']);
    }

}