<?php


namespace app\lucky\pay\model;

use think\Model;
use think\Db;

class UserGoldModel extends Model
{

    private $database = 'database';

/*
     * 获取最近30天的金豆消费记录
     * @param $page_no 当前页
     * @param $page_count 获取多少
     * @param $userId 用户Id
     * @param $sys 系统
     */

    public function getUserGolduserGoldHistory($page_no, $page_count, $userId, $sys)
    {
        $list = Db::database($this->database)
            ->table('user_gold_history')
            ->order('create_time', 'desc')
            ->where('user_id', $userId)
            ->where('sys', $sys);
        $data = $list->page($page_no, $page_count)->select();
        $count = $list->count();

        return ['data' => $data, 'count' => $count];
    }


    /*
     * 获取用户金豆数据
     */
    public function getUserGoldNumber($userId,$sys)
    {
        return Db::database($this->database)
            ->table('user_gold')
            ->where('user_id',$userId)
            ->where('sys',$sys)
            ->field(['create_time','update_time','sys','id'],true)
            ->find();
    }

    /*
     * 根据用户ID,更新用户金币表数据
     * @param $userId 用户ID
     * @param $sys 系统
     * @param $version 乐观锁
     * @param $data 更新的数据
     */
    public function updateUserGold($userId,$data,$version,$sys)
    {
        return Db::database($this->database)
            ->table('user_gold')
            ->where('user_id',$userId)
            ->where('version',$version)
            ->where('sys',$sys)
            ->update($data);
    }

    /*
     * 插入用户金币明细表数据
     * @param $data 用户数据
     */
    public function insertUserMoneyHistory($data)
    {
        return Db::database($this->database)
            ->table('user_gold_history')
           ->strict()->insert($data);
    }

    /*
     * 添加用户的金币与经验信息
     * @param $data 添加到数据
     */

    public function insertUserGold($data)
    {
         return Db::database($this->database)
            ->table('user_gold')
           ->strict()->insert($data);
    }

    /*
     * 添加用户经验明细表数据
     * @data 数据
     */

    public function insertUserExperienceHistory($data)
    {
        return Db::database($this->database)
            ->table('user_experience_history')
           ->strict()->insert($data);
    }

}