<?php


namespace app\lucky\pay\service;

use app\common\Redis;
use think\facade\Log;
use think\Exception;
use app\lucky\pay\model\UserGoldModel;
use app\lucky\user\service\UserService;
use \app\common\InstanceTrait;
use app\sports\certification\service\CertificationService;
use think\Db;

class UserGoldService
{
    use InstanceTrait;
    protected $model = '';

    //分享资讯
    protected $shareInformation = [
        'level' => 15,
        'gold' => 10,
        'num' => 2,
        'msg' => '分享资讯任务',
    ];

    //阅读资讯
    protected $readInformation = [
        'level' => 10,
        'gold' => 3,
        'num' => 5,
        'msg' => '阅读资讯任务',
    ];

    //发表帖子
    protected $publishTopic = [
        'level' => 15,
        'gold' => 10,
        'num' => 2,
        'msg' => '发表帖子任务',
    ];

    //发表评论
    protected $publishComment = [
        'level' => 15,
        'gold' => 3,
        'num' => 5,
        'msg' => '发表评论任务',
    ];

    //聊天室发言
    protected $chatSpeak = [
        'level' => 15,
        'gold' => 2,
        'num' => 10,
        'msg' => '聊天室发言任务'
    ];

    public function __construct()
    {
        $this->model = new UserGoldModel();
    }


  /*
     * 获取用户最近30天金豆消费明细
     * @param $page_no 当前页
     * @param $page_count 获取多少
     * @param $userId 用户Id
     * @param $sys 系统
     */
    public function getUserGoldConsumptionDetail($page_no, $page_count, $userId, $sys)
    {
        $data = $this->model->getUserGolduserGoldHistory($page_no, $page_count, $userId, $sys);
        //处理json格式的数据与时间正常显示
        foreach ($data['data'] as $key => $value) {
            $data['data'][$key]['desc'] = json_decode($value['desc'], true);
            $data['data'][$key]['date'] = date('m-d H:i', $value['create_time']);
        }

        //处理分页逻辑
        //总共记录
        $total_count = $data['count'];
        //返回数据
        $data = $data['data'];
        //总页数
        $total_page = ceil($total_count / $page_count);
        $page_info = [
            'page_no' => $page_no,
            'page_count' => $page_count,
            'page_total' => $total_page,
            'total' => $total_count,
        ];
        return [
            'page_info' => $page_info,
            'list' => $data,
        ];
    }



    /*
     * 获取用户的金币数量与用户经验
     * @param $userId 用户ID
     * @param $sys 系统
     */

    public function getUserGoldAndLevel($userId, $sys)
    {
        $data = $this->model->getUserGoldNumber($userId, $sys);
        //没有查询到数据
        if (!isset($data['experience'])) {
            $data['experience'] = 0;
            $data['num'] = 0;
            $data['version'] = 1;
            $data['user_id'] = $userId;
        }
        $data['level'] = round($data['experience'] / 500) + 1;      //等级
        $data['last_experience'] = abs(($data['experience'] - 500 * $data['level']));        //升级下一级所需经验
        $data['present_experience'] = $data['experience'] - abs($data['level'] - 1) * 500;  //当前经验
        return $data;
    }

    /*
     * 明天凌晨删除任务的key
     */
    public function delEverydayTask()
    {
        $key = 'sports:everyday:task:tate';
        return Redis::getInstance()->redisDel($key);
    }


    /*
     * 读取用户任务进度
     * @param $userId 用户的ID
     * @param $sys 系统类型
     */

    public function getTaskTate($userId, $sys)
    {
        $key = "$sys:everyday:task:tate";
        $arr = [0 => 'shareInformation', 1 => 'readInformation', 2 => 'publishTopic', 3 => 'publishComment', 4 => 'chatSpeak'];
        $data = [];
        for ($i = 0; $i <= 4; $i++) {
            $member = $arr[$i] . '_' . $userId;
            $numberNum = Redis::getInstance()->redisZscore($key, $member);
            $fun = $arr[$i];
            $status = $numberNum >= $this->$fun['num'];
            $data[$arr[$i]] = [
                'num' => $numberNum > $this->$fun['num'] ? $this->$fun['num'] : $numberNum,
                'total_num' => $this->$fun['num'],
                'status' => $status,
            ];
        }
        return $data;
    }

    /*
     * 我的任务
     * @param $userId 用户ID
     * @param $sys 系统
     */

    public function getMyTaskInfo($userId, $sys)
    {
        $data = $this->model->getUserGoldNumber($userId, $sys);
        if (!isset($data['experience'])) {
            $data['user_id'] = $userId;
            $data['num'] = 0;
            $data['experience'] = 0;
        }
        $data['level'] = floor($data['experience'] / 500) + 1;      //等级
        $data['last_experience'] = abs(($data['experience'] - 500 * $data['level']));        //升级下一级所需经验
        $data['present_experience'] = abs($data['experience'] - abs($data['level'] - 1) * 500);  //当前经验
        //获取用户的头像信息
        $userInfo = array_merge(UserService::getInstance()->getUserInfo($userId, $sys), $data);

        //每日任务
        $taskInfo = $this->getTaskTate($userId, $sys);

        //其他任务
        $otherTasksArr = [];
        //实名认证
        $certificationService = new CertificationService;
        $auditFlag = $certificationService->isAudited($userId, $sys);
        $otherTasksArr['realNameAuthentication'] = $auditFlag;

        return ['userinfo' => $userInfo, 'taskInfo' => $taskInfo, 'otherTasks' => $otherTasksArr];
    }


    /*
     * 实名认证成功调用的接口  目前,产品暂定,实名认证通过+50金豆，增加100经验值；一般只调用一次
     * @param $userId 用户的ID
     * @param $sys 系统
     */

    public function authentication($userId, $sys)
    {

        return $this->performTask($userId, 100, 50, '实名认证', $sys);

    }

    /*
     * 分享资讯  分享资讯+10金豆，每日任务2次，完成任务获得15点经验值 ,共计增加20金豆
     * @param $userId 用户的ID
     * @param $sys 系统
     */
    public function shareInformation($userId, $sys)
    {
        $key = "$sys:everyday:task:tate";
        $member = 'shareInformation' . '_' . $userId;
        //小于次数,就增加金豆,并且自增1
        Redis::getInstance()->redisZincrby($key, 1, $member);
        $number = Redis::getInstance()->redisZscore($key, $member);
        if ($number == $this->shareInformation['num']) {
            //区别是第一次还是第二次
            $lock = $member . 'ok';
            $lockNumber = Redis::getInstance()->redisZscore($key, $lock);
            if (!empty($lockNumber)) {
                return [];
            }
            Redis::getInstance()->redisZincrby($key, 1, $lock);
            //到达次数,就增加金豆与经验
            return $this->performTask($userId, $this->shareInformation['level'], $this->shareInformation['gold'], $this->shareInformation['msg'], $sys);
        } elseif ($number < $this->shareInformation['num']) {
            return $this->performTask($userId, 0, $this->shareInformation['gold'], $this->shareInformation['msg'], $sys);
        }
    }

    /*
     * 阅读资讯 阅读资讯+3金豆，每日任务5次，完成任务获得10点经验值；共计 15金豆
     * @param $userID 用户的ID
     * @param $sys 系统
     */

    public function readInformation($userId, $sys)
    {
        $key = "$sys:everyday:task:tate";
        $member = 'readInformation' . '_' . $userId;
        //小于次数,就增加金豆,并且自增1
        Redis::getInstance()->redisZincrby($key, 1, $member);
        $number = Redis::getInstance()->redisZscore($key, $member);
        if ($number == $this->readInformation['num']) {
            //区别是第一次还是第二次
            $lock = $member . 'ok';
            $lockNumber = Redis::getInstance()->redisZscore($key, $lock);
            if (!empty($lockNumber)) {
                return [];
            }
            Redis::getInstance()->redisZincrby($key, 1, $lock);
            //到达次数,就增加金豆与经验
            return $this->performTask($userId, $this->readInformation['level'], $this->readInformation['gold'], $this->readInformation['msg'], $sys);
        } elseif ($number < $this->readInformation['num']) {
            return $this->performTask($userId, 0, $this->readInformation['gold'], $this->readInformation['msg'], $sys);
        }
    }


    /*
     * 发布帖子 发布帖子+10金豆，每日任务2次，完成任务获得15点经验值；
     * @param $userID 用户的ID
     * @param $sys 系统
     */

    public function publishTopic($userId, $sys)
    {
        $key = "$sys:everyday:task:tate";
        $member = 'publishTopic' . '_' . $userId;
        //小于次数,就增加金豆,并且自增1
        Redis::getInstance()->redisZincrby($key, 1, $member);
        $number = Redis::getInstance()->redisZscore($key, $member);
        if ($number == $this->publishTopic['num']) {
            //区别是第一次还是第二次
            $lock = $member . 'ok';
            $lockNumber = Redis::getInstance()->redisZscore($key, $lock);
            if (!empty($lockNumber)) {
                return [];
            }
            Redis::getInstance()->redisZincrby($key, 1, $lock);
            //到达次数,就增加金豆与经验
            return $this->performTask($userId, $this->publishTopic['level'], $this->publishTopic['gold'], $this->publishTopic['msg'], $sys);
        } elseif ($number < $this->publishTopic['num']) {
            return $this->performTask($userId, 0, $this->publishTopic['gold'], $this->publishTopic['msg'], $sys);
        }
    }

    /*
     * 发布评论 发布评论+3金豆，每日任务5次，完成任务获得15点经验值；
     * @param $userID 用户的ID
     * @param $sys 系统
     */

    public function publishComment($userId, $sys)
    {
        $key = "$sys:everyday:task:tate";
        $member = 'publishComment' . '_' . $userId;
        //小于次数,就增加金豆,并且自增1
        Redis::getInstance()->redisZincrby($key, 1, $member);
        $number = Redis::getInstance()->redisZscore($key, $member);
        if ($number == $this->publishComment['num']) {
            //区别是第一次还是第二次
            $lock = $member . 'ok';
            $lockNumber = Redis::getInstance()->redisZscore($key, $lock);
            if (!empty($lockNumber)) {
                return [];
            }
            Redis::getInstance()->redisZincrby($key, 1, $lock);
            //到达次数,就增加金豆与经验
            return $this->performTask($userId, $this->publishComment['level'], $this->publishComment['gold'], $this->publishComment['msg'], $sys);
        } elseif ($number < $this->publishComment['num']) {
            return $this->performTask($userId, 0, $this->publishComment['gold'], $this->publishComment['msg'], $sys);
        }
    }

    /*
     * 聊天室发言 每日任务10次， 每次2金豆,完成任务获得15点经验值；总计20金豆
     * @param $userID 用户的ID
     * @param $sys 系统
     */

    public function chatSpeak($userId, $sys)
    {
        $key = "$sys:everyday:task:tate";
        $member = 'chatSpeak' . '_' . $userId;
        //小于次数,就增加金豆,并且自增1
        Redis::getInstance()->redisZincrby($key, 1, $member);
        $number = Redis::getInstance()->redisZscore($key, $member);
        if ($number == $this->chatSpeak['num']) {
            //区别是第一次还是第二次
            $lock = $member . 'ok';
            $lockNumber = Redis::getInstance()->redisZscore($key, $lock);
            if (!empty($lockNumber)) {
                return [];
            }
            Redis::getInstance()->redisZincrby($key, 1, $lock);
            //到达次数,就增加金豆与经验
            return $this->performTask($userId, $this->chatSpeak['level'], $this->chatSpeak['gold'], $this->chatSpeak['msg'], $sys);
        } elseif ($number < $this->chatSpeak['num']) {
            return $this->performTask($userId, 0, $this->chatSpeak['gold'], $this->chatSpeak['msg'], $sys);
        }
    }

    /*
     * 完成任务增加金币与经验
     * @param $userId 用户id
     * @param $level 经验值
     * @param $gold 金币
     * @param $msg 完成任务名称
     * @param sys 系统
     */

    public function performTask($userId, $level, $gold, $msg, $sys)
    {
        Db::startTrans();
        try {
            $userInfo = $this->model->getUserGoldNumber($userId, $sys);
            //处理金币表与经验表user_gold
            if (empty($userInfo)) {
                //第一次要添加
                $userGold = [
                    'num' => $gold,
                    'update_time' => time(),
                    'experience' => $level,
                    'version' => 1,
                    'create_time' => time(),
                    'sys' => $sys,
                    'user_id' => $userId,
                ];
                $this->model->insertUserGold($userGold);
            } else {
                $version = $userInfo['version'];
                $userGold = [
                    'num' => $userInfo['num'] + $gold,
                    'update_time' => time(),
                    'experience' => $userInfo['experience'] + $level,
                    'version' => $version + 1,
                ];
                $this->model->updateUserGold($userId, $userGold, $version, $sys);
            }

            //处理金币明细表user_gold_history
            $last_num = isset($userInfo['num']) ? $userInfo['num'] : 0;
            $userGoldHistory = [
                'user_id' => $userId,
                'change_num' => $gold,
                'remain_num' => $last_num + $gold,
                'desc' => json_encode([
                    'type' => 0,
                    'msg' => $msg,
                ], JSON_UNESCAPED_UNICODE),
                'create_time' => time(),
                'sys' => $sys,
                'type' => 0,
            ];
            $this->model->insertUserMoneyHistory($userGoldHistory);

            //处理经验明细表,user_experience_history
            if ($level > 0) {
                $addUserExperienceHistory = [
                    'user_id' => $userId,
                    'change_experience' => $level,
                    'remain_experience' => $userInfo['experience'] + $level,
                    'desc' => $msg,
                    'create_time' => time(),
                    'sys' => $sys,
                ];
                $this->model->insertUserExperienceHistory($addUserExperienceHistory);
            }
            Db::commit();
            return true;
        } catch (Exception $e) {
            Db::rollback();
            $err = json_encode([
                'line' => $e->getLine(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'msg' => $e->getMessage(),
            ], JSON_UNESCAPED_UNICODE);
            Log::error('完成任务增加金币与经验发生错误!' . $err);
            return false;
        }
    }


}