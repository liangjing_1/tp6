<?php
//频道

namespace app\lucky\admin\controller;

use app\lucky\admin\service\ChannelService;
use app\common\DataValidate;
use app\lucky\channel\validate\ChannelValidate;
use think\App;
use think\Request;

class Channel extends AdminBase
{
    public $service = null;
    public $sys = '';

    public function __construct(App $app = null, Request $request = null)
    {
        parent::__construct($app, $request);
        $this->service = new channelService();
        $this->sys = \session('sys') ? \session('sys') : 343;
    }

    /**
     * @description  列表
     * 路由：可访问 /admin/channel/list
     */
    public function getList()
    {
        $postData = $this->request->post();
        $valiData = DataValidate::getInstance()->checkPager($postData);
        if ($valiData["code"] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($valiData["code"], $valiData["msg"]);
        }

        $postData = $valiData['data'];
        $data = $this->service->getList($postData, $this->sys);

        $this->_success($data);
    }

    /**
     * @description  获取所有列表
     * 路由：可访问 /admin/channel/alllist
     */
    public function getAllList()
    {
        $channelService = new \app\lucky\channel\service\ChannelService();
        $data = $channelService->getAllList($this->sys);

        $this->_success($data);
    }


    /**
     * @description  创建
     * 路由：可访问 /admin/channel/create
     */
    public function create()
    {
        $postData = $this->request->post();
        $valiData = ChannelValidate::getInstance()->checkAdd($postData);
        if ($valiData["code"] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($valiData["code"], $valiData["msg"]);
        }

        $postData = $valiData['data'];
        $postData['admin_id'] = $this->loginInfo['admin_id'];
        $result = $this->service->create($postData, $this->sys);

        if ($result['code'] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($result['code'], $result["msg"]);
        }

        $this->_success([], $result["msg"]);
    }

    /**
     * @description 修改 
     * 路由：可访问 /admin/channel/update
     */
    public function update()
    {
        $postData = $this->request->post();
        $valiData = ChannelValidate::getInstance()->checkUpdate($postData);
        if ($valiData["code"] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($valiData["code"], $valiData["msg"]);
        }

        $postData = $valiData['data'];

        $result = $this->service->update($postData['channel_id'], $postData, $this->sys);

        if ($result['code'] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($result['code'], $result["msg"]);
        }
        $this->_success($result["data"], $result["msg"]);
    }


    /**
     * @description 开启关闭
     * 路由：可访问 /admin/channel/is_show
     * 是否开启0不1是，开启后展示在资讯频道编辑的频道推荐
     */
    public function updateIsShow()
    {
        $postData = $this->request->post();
        $valiData = ChannelValidate::getInstance()->updateIsShow($postData);
        if ($valiData["code"] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($valiData["code"], $valiData["msg"]);
        }

        $postData = $valiData['data'];

        $result = $this->service->updateIsShow($postData['channel_id'], $postData['is_show'], $this->sys);

        if ($result['code'] != _MSG_SYSTEM_SUCCESS) {
            $this->_error($result['code'], $result["msg"]);
        }
        $this->_success($result["data"], $result["msg"]);
    }

    /**
     * @description 删除
     * 路由：可访问 /admin/channel/delete
     */
    public function delete()
    {
        $post = $this->request->post();
        if (!isset($post["channel_id"])) {
            $this->_error(_MSG_INVALID_CLIENT_PARAM, "必须提供 channel_id");
        }

        $result = $this->service->delete($post["channel_id"], $this->sys);

        if (!$result) {
            $this->_error(_MSG_NOT_ALLOWED_METHOD, "删除失败");
        }
        $this->_success([], "删除成功");
    }


}