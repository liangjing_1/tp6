<?php


namespace app\lucky\admin\controller;

use \app\lucky\admin\controller\AdminBase;
use think\App;
use app\common\Redis;
use \app\common\InstanceTrait;
use app\lucky\admin\service\WxMenuService;
use think\Request;

class WxMenu extends AdminBase
{
    private $service = [];
    public $sys = '';
    protected $authCheck = false;
    protected $headerCheck = false;

    public function __construct(App $app = null, Request $request)
    {
        parent::__construct($app, $request);
        $this->service = new WxMenuService;
        $this->sys = session('sys');
    }

    /*
     * 获取微信菜单列表
     */
    public function getWxMenuList()
    {
        $this->checkLogin();
        $res = $this->service->listMenu();
        if ($res) {
            $this->_success($res);
        } else {
            $this->_error(-1, '获取微信菜单列表失败!');
        }

    }

    /*
     * 创建微信菜单
     */
    public function createWxMenu()
    {
        $this->checkLogin();
        $data = $this->request->post();
        if (empty($data['button'])) {
            $this->_error(-1, '请输入参数!');
        }
        $res = $this->service->createWxMenu(json_encode($data['button'], JSON_UNESCAPED_UNICODE));
        if ($res['errcode'] == 0) {
            $this->_success($res);
        } else {
            $this->_error(-1, '创建微信菜单列表失败!');
        }
    }


    /*
     * 主动模式与被动模式
     */
    public function replyMessess()
    {
        $data = $this->request->get();
        if (empty($data['echostr'])) {
            return $this->service->acceptMsg();
        } else {
            //第一次
            return $this->service->checkSignature($data);
        }
    }

 /*
     * 删除菜单
     */
    public function deleteWxMenu()
    {
        $this->checkLogin();
        $data = $this->request->post();
        if (empty($data['success'])) {
            $this->_error(-1, '请传入参数!');
        }
        if ($data['success'] == 1) {
            return $this->service->delMenu();
        }
    }


    /*
     * 上传素材
     */
    public function update()
    {
        $this->checkLogin();
        $data = $this->request->file('file');
        //先留着,本来想一起写的

    }

}