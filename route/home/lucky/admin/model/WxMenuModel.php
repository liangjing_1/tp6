<?php


namespace app\lucky\admin\model;

use think\Db;
use think\Model;

class WxMenuModel extends Model
{
    /*
     * 插入文字回复消息
     */

    public function insertText($data)
    {
        return Db::table('wx_text')->insertGetId($data);
    }

    /*
     * 查询事件对应的消息
     */

    public function getText($id)
    {
         return Db::table('wx_text')->where('id',$id)->find();
    }
}