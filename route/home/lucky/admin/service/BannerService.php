<?php
/**
 * Banner
 */

namespace app\lucky\admin\service;

use app\hook\adminLog\context\AdminLogContext;
use app\hook\adminLog\controller\AdminLog;
use app\lucky\blog\model\BlogModel;
use app\lucky\banner\model\BannerModel;
use think\Db;

class BannerService
{
    //表名
    const ADMIN_OP_TABLE_NAME = 'banners';

    /*
     * 列表
     */
    public function getList($data, $sys = 343)
    {
        $page = isset($data['page_no']) ? $data['page_no'] : 1;
        $pageCount = isset($data['page_count']) ? $data['page_count'] : 10;
        $searchName = isset($data['name']) ? $data['name'] : '';

        $result = BannerModel::getInstance()->selectStatusListByPager($page, $pageCount, $searchName, $sys);
        $data = $result['data'];

        //处理前端数据格式

        $pageTotal = (int)ceil($result['total'] / $pageCount);
        $page_info = [
            'total' => $result['total'],//总记录
            'page_no' => $page,//页数
            'page_count' => $pageCount,//每页显示数量
            'page_total' => $pageTotal,//总页数
        ];

        return [
            'page_info' => $page_info,
            'list' => $data,
        ];
    }

    /**
     * 添加
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function create($data, $sys = 343)
    {
        $data = [
            "name" => $data['name'],
            "direction" => $data['direction'],
            "img_url" => $data['img_url'],
            "status" => $data['status'],
            "rank" => $data['rank'],
            "desc" => isset($data['desc']) ? $data['desc'] : '',
            "link_url" => isset($data['link_url']) ? $data['link_url'] : '',
            "content" => isset($data['content']) ? $data['content'] : '',
            "create_time" => date('Y-m-d H:i:s', time()),
            "sys" => $sys
        ];

        $result = BannerModel::getInstance()->insertGetId($data);

        if ($result) {
            //添加操作日志
            $opInfo = '添加banner';
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_CREATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $result, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "添加成功", "data" => []];
        } else {
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "添加失败", "data" => []];
        }
    }

    /**
     * 修改
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function edit($bannerId, $data, $sys = 343)
    {
        $banner = BannerModel::getInstance()->getOneById($bannerId, $sys);
        if (!$banner) {
            return ["code" => _MSG_NOT_FOUND, "msg" => "数据不存在", "data" => []];
        }

        $opInfo = '修改信息';
        $update = [
            "name" => $data['name'],
            "direction" => $data['direction'],
            "img_url" => $data['img_url'],
            "status" => $data['status'],
            "rank" => $data['rank'],
            "desc" => isset($data['desc']) ? $data['desc'] : '',
            "create_time" => date('Y-m-d H:i:s', time()),
            "sys" => $sys
        ];

        if (isset($data['link_url'])){
            $update['link_url'] = $data['link_url'];
            $update['content'] = '';
        }
        if (isset($data['content'])){
            $update['content'] = $data['content'];
            $update['link_url'] = '';
        }

        $result = BannerModel::getInstance()->update($bannerId, $update, $sys);

        if ($result) {
            //添加操作日志
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_UPDATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $bannerId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "操作成功", "data" => []];
        } else {
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "操作失败", "data" => []];
        }
    }

    /**
     * 修改
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function updateIsShow($bannerId, $isShow, $sys = 343)
    {
        //数据是否存在
        $banner = BannerModel::getInstance()->getOneById($bannerId, $sys);
        if (!$banner) {
            return ["code" => _MSG_NOT_FOUND, "msg" => "数据不存在", "data" => []];
        }

        if ($isShow) {
            $opInfo = '开启';
        } else {
            $opInfo = '关闭';
        }

        $update = [
            'update_time' => time(),
            'is_show' => $isShow,
            "sys" => $sys
        ];

        $result = BannerModel::getInstance()->update($bannerId, $update, $sys);

        if ($result) {
            //添加操作日志
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_UPDATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $bannerId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "操作成功", "data" => []];
        } else {
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "操作失败", "data" => []];
        }
    }

    /**
     * 删除
     */
    public function delete($bannerId, $sys = 343)
    {
        $result = BannerModel::getInstance()->delete($bannerId, $sys);

        if ($result) {
            //添加操作日志
            $opInfo = '删除banner';
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_DELETE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $bannerId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);
        }

        return $result;
    }

    /**
     * 信息
     */
    public function getInfo($bannerId, $sys = 343)
    {
        $bannerInfo = BannerModel::getInstance()->getOneById($bannerId, $sys);

        if (!$bannerInfo) {
            return ["code" => _MSG_NOT_FOUND, "msg" => "查询失败", "data" => []];
        }

        //处理前端数据格式
        $bannerInfo['content'] = htmlspecialchars_decode($bannerInfo['content']);
        $bannerInfo['direction_name'] = $bannerInfo['direction'] == 'app_findindex' ? '发现首页' : '';

        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "查询成功", "data" => $bannerInfo];
    }

    /**
     * 修改banner状态
     */
    public function updateBannerStatus($bannerId, $status, $sys = 343)
    {

        $result = BannerModel::getInstance()->updateBannerStatus($bannerId, $status, $sys);

        if ($result) {
            //添加操作日志
            $opInfo = '修改banner状态';
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_DELETE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $bannerId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);
        }

        return $result;
    }


}