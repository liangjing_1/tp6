<?php
/**
 *资讯
 */

namespace app\lucky\admin\service;

use app\common\DataService;
use app\hook\adminLog\context\AdminLogContext;
use app\hook\adminLog\controller\AdminLog;
use app\lucky\author\model\AuthorModel;
use app\lucky\author\service\AuthorService;
use app\lucky\blog\model\BlogInstanceModel;
use app\lucky\blog\model\BlogModel;
use app\lucky\channel\model\ChannelModel;
use app\lucky\common\BaiduTui;
use app\lucky\common\InstanceTrait;
use app\lucky\common\Lottery;
use app\lucky\common\OSS;
use app\lucky\common\Redis;
use app\lucky\follow\service\FollowService;
use app\lucky\like\service\LikeService;
use app\lucky\lottery\service\LotteryService;
use app\lucky\push\service\PushService;
use think\Db;
use Log;

class BlogInstanceService
{
    use InstanceTrait;

    //表名
    const ADMIN_OP_TABLE_NAME = 'blog_instance';

    /*
     * 列表
     */
    public function getList($data, $sys = 343)
    {
        $page = isset($data['page_no']) ? $data['page_no'] : 1;
        $pageCount = isset($data['page_count']) ? $data['page_count'] : 10;
        //查找文章标题
        $searchBlogTitle = isset($data['search']) ? $data['search'] : '';
        //查找作者名称
        $searchAuthorName = isset($data['search_user']) ? $data['search_user'] : '';
        if ($searchAuthorName) {
            $searchAuthorIdArr = AuthorModel::getInstance()->getAuthorIdArrByLikeAuthorName($searchAuthorName, $sys);
            $searchAuthorIdArr = $searchAuthorIdArr ? $searchAuthorIdArr : [-1];
        } else {
            $searchAuthorIdArr = [];
        }
//        field排序字段lifting1升序，0降序
        $field = isset($data['field']) ? $data['field'] : '';
        $lifting = isset($data['lifting']) ? $data['lifting'] : 1;
        //是否推荐1是0否
        $isRecommend = isset($data['is_recommend']) ? $data['is_recommend'] : '';
        //频道id（传channel_id）
        $channelId = isset($data['channel_id']) ? $data['channel_id'] : '';

        $result = BlogInstanceModel::getInstance()->getBlogListByPager($page, $pageCount, $searchBlogTitle, $searchAuthorIdArr, $field, $lifting, $sys, $isRecommend, $channelId);
        $data = $result['data'];

        //处理前端数据格式
        $likeService = new LikeService();
        if ($data) {
            foreach ($data as $k => $v) {
                $authorInfo = AuthorModel::getInstance()->getOneById($v['author_id'], $sys);
                $authorName = isset($authorInfo['author_name']) ? $authorInfo['author_name'] : '';
                $data[$k]['author_name'] = $authorName;
                $data[$k]['blog_likes'] = $likeService->getBlogLikeCount($v['blog_id'], $sys)['count'];
                //频道名
                $channelInfo = ChannelModel::getInstance()->getOneById($v['channel_id'], $sys);
                $data[$k]['channel_name'] = isset($channelInfo['name']) ? $channelInfo['name'] : '';
            }
        }

        $pageTotal = (int)ceil($result['total'] / $pageCount);
        $page_info = [
            'total' => $result['total'],//总记录
            'page_no' => $page,//页数
            'page_count' => $pageCount,//每页显示数量
            'page_total' => $pageTotal,//总页数
        ];

        return [
            'page_info' => $page_info,
            'list' => $data,
        ];
    }

    /**
     * 信息
     */
    public function getInfo($blogId, $sys = 343)
    {
        //资讯信息
        $blogInfo = BlogInstanceModel::getInstance()->getOneBlogById($blogId, $sys);
        if (!$blogInfo) {
            return ["code" => _MSG_NOT_FOUND, "msg" => "查询失败", "data" => []];
        }
        //作者信息
        $authorInfo = AuthorModel::getInstance()->getOneById($blogInfo['author_id'], $sys);
        $blogInfo['author_name'] = isset($authorInfo['author_name']) ? $authorInfo['author_name'] : '';
        //频道信息
        $channelInfo = ChannelModel::getInstance()->getOneById($blogInfo['channel_id'], $sys);
        $blogInfo['channel_name'] = isset($channelInfo['name']) ? $channelInfo['name'] : '';
        $blogInfo['create_time'] = date('Y-m-d H:i:s', $blogInfo['created_at']);
        $blogInfo['is_recommend_name'] = $blogInfo['is_recommend'] == 1 ? '是' : '否';
        $blogInfo['content'] = htmlspecialchars_decode($blogInfo['content']);

        //资讯真实点赞量
        $likeService = new LikeService();
        $blogInfo['blog_likes'] = $likeService->getBlogLikeCount($blogId, $sys)['count'];

        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "查询成功", "data" => $blogInfo];
    }

    /**
     * 编辑资讯
     */
    public function updateBlog($blogId, $data, $sys = 343)
    {
        $blogOldInfo = BlogInstanceModel::getInstance()->getOneBlogById($blogId, $sys);
        if (!$blogOldInfo) {
            return ["code" => _MSG_NOT_FOUND, "msg" => "查询失败", "data" => []];
        }

        $update = [
            'updated_at' => time(),
            'admin_id' => $data['admin_id'],
            "sys" => $sys
        ];

        if (isset($data['title'])) {
            $update['title'] = $data['title'];
        }
        if (isset($data['content'])) {
            $update['content'] = htmlspecialchars($data['content']);
        }
        if (isset($data['blog_likes_fake'])) {
            $update['blog_likes_fake'] = $data['blog_likes_fake'];
        }
        if (isset($data['thumbnail_url'])) {
            if ($data['thumbnail_url'] != $blogOldInfo["thumbnail_url"]) {
                $update['thumbnail_url'] = $data['thumbnail_url'];
            }
        }
        if (isset($data['is_recommend'])) {
            $update['is_recommend'] = $data['is_recommend'];
        }
        if (isset($data['channel_id'])) {
            $update['channel_id'] = $data['channel_id'];
        }

        $result = BlogInstanceModel::getInstance()->updateBlog($blogId, $update, $sys);

        if ($result) {
            //删除旧图
            if (isset($update['thumbnail_url'])) {
                OSS::getInstance()->deleteFile($blogOldInfo["thumbnail_url"]);
            }

            //添加操作日志
            $opInfo = '修改资讯文章';
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_UPDATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $blogId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "操作成功", "data" => []];
        } else {
            //删除新图
            if (isset($update['thumbnail_url'])) {
                OSS::getInstance()->deleteFile($data["thumbnail_url"]);
            }
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "操作失败", "data" => []];
        }

    }

    /**
     * 修改资讯推荐
     */
    public function updateBlogIsRecommend($blogId, $data, $sys = 343)
    {
        $blogOldInfo = BlogInstanceModel::getInstance()->getOneBlogById($blogId, $sys);
        if (!$blogOldInfo) {
            return ["code" => _MSG_NOT_FOUND, "msg" => "查询失败", "data" => []];
        }

        $update = [
            'updated_at' => time(),
            'admin_id' => $data['admin_id'],
            "sys" => $sys
        ];

        if (isset($data['is_recommend'])) {
            $update['is_recommend'] = $data['is_recommend'];
        }

        $result = BlogInstanceModel::getInstance()->updateBlog($blogId, $update, $sys);

        if ($result) {
            //添加操作日志
            $opInfo = '修改资讯推荐';
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_UPDATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $blogId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "操作成功", "data" => []];
        } else {
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "操作失败", "data" => []];
        }

    }

    /**
     * 添加后台
     */
    public function createBlog($data, $sys = 343)
    {
        $data = [
            "admin_id" => $data['admin_id'],
            "author_id" => $data['author_id'],
            "type_id" => 4,
            "title" => $data['title'],
            "content" => htmlspecialchars($data['content']),
            "is_recommend" => isset($data['is_recommend']) ? $data['is_recommend'] : 0,
            "channel_id" => isset($data['channel_id']) ? $data['channel_id'] : 0,
            "blog_likes_fake" => isset($data['blog_likes_fake']) ? $data['blog_likes_fake'] : 0,
            "thumbnail_url" => isset($data['thumbnail_url']) ? $data['thumbnail_url'] : '',
            "created_at" => time(),
            "sys" => $sys
        ];


        $result = BlogInstanceModel::getInstance()->insertBlog($data);

        if ($result) {
            //添加操作日志
            $opInfo = '添加资讯文章';
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_CREATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $result, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            //获取作者信息
            $authorService = new AuthorService();
            $authorInfo = $authorService->getAuthorInfoById($data['author_id'], $sys);
            if ($authorInfo) {
                //判断是否进行APP PUSH
                $title = $authorInfo['author_name'] . '发布资讯';
                $pushService = new PushService();
                $pushService->blogPush($data['author_id'], $title, mb_substr($data['title'], 0, 10) . '...', $result);
            }

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "添加成功", "data" => []];
        } else {
            if (isset($data['author_headimgurl'])) {
                OSS::getInstance()->deleteFile($data["author_headimgurl"]);
            }
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "添加失败", "data" => []];
        }
    }

    //TODO  创建文章
    public function create($data, $sys = 343)
    {

        $homeShow = false;
        if (isset($data["home_page_show"])) {
            if ($data["home_page_show"]) {
                $data["status"] = 1;
                $type = $data['type_id'];
                $homeShow = true;
            }
        }

        if ($data["type_id"] == 2) {
            if (!isset($data["lottery_id"])) {
                return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => "彩种ID必填"];
            }
            if (!LotteryService::getInstance()->checkLotteryId($data["lottery_id"])) {
                return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => "暂不支持该彩种预测"];
            }
            if (!isset($data["lottery_no"])) {
                return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => "期号必填"];
            }
            if (!LotteryService::getInstance()->checkLotteryNo($data["lottery_id"], $data["lottery_no"])) {
                return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => "期号格式错误"];
            }
        } else {
            $data["lottery_id"] = "";
            $data["lottery_no"] = "";
        }

        if ((int)$data["status"] == 1 && isset($data["publish_at"])) {
            $data["publish_at"] = strtotime($data["publish_at"]);
            if ($data["publish_at"] <= time() + 5 * 60) {
                return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => "定时发布延时必须超过5分钟"];
            }
        }

        if (isset($data["user_id"])) {
            $item = DB::table("user_000")
                ->where("user_id", $data["user_id"])->where("sys", $sys)->find();
            if (empty($item)) {
                return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => "虚拟用户不存在"];
            }
            $data["user_name"] = $item["nickname"];
        }


        $result = BlogInstanceModel::getInstance()->insert($data, $sys);
        if ($result["code"] != _MSG_SYSTEM_SUCCESS) {
            return $result;
        }

        // **** 百度链接推送
        $route = "/";
        if ((int)$data["type_id"] == 2) {
            switch ($data["lottery_id"]) {
                case "ssq":
                    $route .= "ssq";
                    break;
                case "fcsd":
                    $route .= "3d";
                    break;
                case "dlt":
                    $route .= "dlt";
                    break;
                case "pls":
                    $route .= "pls";
                    break;
                case "plw":
                    $route .= "plw";
                    break;
                case "qxc":
                    $route .= "qxc";
                    break;
                case "qlc":
                    $route .= "qlc";
                    break;
                default:
                    $route .= "ssq";
            };
            $route .= "/" . $result["data"]["blog_id"] . ".html";
        } elseif ((int)$data["type_id"] == 1) {
            $route = "/news/" . $result["data"]["blog_id"] . ".html";
        }
        BaiduTui::getInstance()->push_($route);
        // ****

        //TODO  问题代码
        // 发布至首页
        if (isset($type)) {
            if ((int)$type == 2) {
                BlogHomePageService::getInstance()->addByLotteryByCreate($type, [$result["data"]["blog_id"]], $data["lottery_id"]);
            } else {
                BlogHomePageService::getInstance()->add($type, [$result["data"]["blog_id"]]);
            }
        }

        if ($homeShow && $data["type_id"]) {
            if ((int)$data["type_id"] == 2) {
                $res = BlogHomePageService::getInstance()->addByLottery($data["type_id"], [$result["data"]["blog_id"]], $data["lottery_id"]);
            } else {
                BlogHomePageService::getInstance()->add($data["type_id"], [$result["data"]["blog_id"]]);
            }
        }

        if ((int)$data["type_id"] == 2 && isset($data["lottery_id"])) {
            BlogHomePageService::getInstance()->addByLotteryByCreate(2, $result["data"]["blog_id"], $data["lottery_id"]);
        }
        if (isset($res) && $res['code'] != _MSG_SYSTEM_SUCCESS) {
            $result["msg"] = "文章发布成功，但热门文章已满十条，热门展示不成功。";
        }
        return $result;
    }

    //TODO 编辑文章
    public function update($updateData, $sys = 343)
    {

        $data = [];
        $data["sys"] = $sys;
        $data["blog_id"] = $updateData["blog_id"];
        $data["updated_at"] = time();
        if (isset($updateData["title"])) {
            $data["title"] = $updateData["title"];
        }
        if (isset($updateData["intro"])) {
            $data["intro"] = $updateData["intro"];
        }
        if (isset($updateData["content"])) {
            $data["content"] = htmlspecialchars($updateData["content"]);
        }
        if (isset($updateData["blog_likes_fake"])) {
            $data["blog_likes_fake"] = $updateData["blog_likes_fake"];
        }
        if (isset($updateData["blog_views_fake"])) {
            $data["blog_views_fake"] = $updateData["blog_views_fake"];
        }
        if (isset($updateData["user_name"])) {
            $data["user_name"] = $updateData["user_name"];
        }
        if (isset($updateData["user_id"])) {
            $data["user_id"] = $updateData["user_id"];
        }
        if (isset($updateData["status"])) {
            $data["status"] = $updateData["status"];
        }
        if (isset($updateData["thumbnail_url"])) {
            $data["thumbnail_url"] = $updateData["thumbnail_url"];
        }
        if (isset($updateData["keywords"])) {
            $data["keywords"] = $updateData["keywords"];
        }

        if ($updateData["type_id"] == 2) {
            if (isset($updateData["lottery_id"])) {

                if (!LotteryService::getInstance()->checkLotteryId($updateData["lottery_id"])) {
                    return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => "暂不支持该彩种预测"];
                }
                if (!isset($updateData["lottery_no"])) {
                    return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => "期号未传"];
                }
                if (!LotteryService::getInstance()->checkLotteryNo($updateData["lottery_id"], $updateData["lottery_no"])) {
                    return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => "期号格式错误"];
                }

                $data["lottery_id"] = $updateData["lottery_id"];
                $data["lottery_no"] = $updateData["lottery_no"];
            }
        } else {
            unset($updateData["lottery_id"]);
            unset($updateData["lottery_no"]);
        }

        $homeShow = false;
        if (isset($updateData["home_page_show"])) {
            if ($updateData["home_page_show"]) {
                $data["status"] = 1;
                $homeShow = true;
            }
        }

        if (isset($updateData["status"]) && (int)$updateData["status"] == 1 && isset($updateData["publish_at"])) {
            $updateData["publish_at"] = strtotime($updateData["publish_at"]);
            if ($updateData["publish_at"] <= time() + 5 * 60) {
                return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => "定时发布延时必须超过5分钟"];
            }
            $data["publish_at"] = $updateData["publish_at"];
        } else {
            $data["publish_at"] = time();
        }

        $result = BlogInstanceModel::getInstance()->update($data);
        if ($result["code"] != _MSG_SYSTEM_SUCCESS) {
            return $result;
        }

        // 发布至首页
        if ($homeShow) {
            if ((int)$updateData["type_id"] == 2) {
                $res = BlogHomePageService::getInstance()->addByLottery($updateData["type_id"], [$result["data"]["blog_id"]], $data["lottery_id"]);
            } else {
                BlogHomePageService::getInstance()->add($updateData["type_id"], [$result["data"]["blog_id"]]);
            }
        }

        if ((int)$updateData["type_id"] == 2 && isset($data["lottery_id"])) {
            //$dataTemp[$data['blog_id']] = $data;
            //$dataTemp[$data['blog_id']] = serialize($dataTemp[$data['blog_id']]);
            //echo _HOME_PAGE_BLOG_10. "2_" .$updateData["lottery_id"];
            //Redis::getInstance()->redisHMSet(_HOME_PAGE_BLOG_10. "2_" .$updateData["lottery_id"],$dataTemp );
            //BlogHomePageService::getInstance()->reloadHomePage($typeId = 2);
            BlogHomePageService::getInstance()->addByLotteryByCreate(2, $data['blog_id'], $data["lottery_id"]);
        }
        if (isset($updateData["type_id"]) && (int)$updateData["type_id"] == 2 && isset($data["lottery_id"])) {
            BlogHomePageService::getInstance()->addByLotteryByCreate(2, $result["data"]["blog_id"], $data["lottery_id"]);
        }
        if (isset($res) && $res['code'] != _MSG_SYSTEM_SUCCESS) {
            $result["msg"] = "文章发布成功，但热门文章已满十条，热门展示不成功。";
        }
        return $result;
    }

    public function listByPager($data, $status, $is_home_show = false, $sys = 343, $isSource = 0)
    {

        if ((int)$data["type_id"] == 1 || (int)$data["type_id"] == 3 || (int)$data["type_id"] == 4) {
            $blogs = Redis::getInstance()->redisHgetKeysAsKey(_HOME_PAGE_BLOG . $data["type_id"]);
            $contains = [];
            if ($is_home_show) {
                if ($blogs['code'] == _MSG_SYSTEM_SUCCESS) {
                    foreach ($blogs['data'] as $blog) {
                        $blog = unserialize($blog);
                        if (isset($blog['blog_id']) && !in_array($blog['blog_id'], $contains)) {
                            $contains[] = $blog['blog_id'];
                        }
                    }
                }
            }

            $result = BlogInstanceModel::getInstance()->selectStatusListByPager([
                "page_no" => isset($data["page_no"]) ? (int)$data["page_no"] : 1,
                "page_count" => isset($data["page_count"]) ? (int)$data["page_count"] : 10,
                "search" => isset($data["search"]) ? $data["search"] : "",
                "search_user" => isset($data["search_user"]) ? $data["search_user"] : "",
                "order" => isset($data["order"]) ? (int)$data["order"] : 0,
                "lifting" => isset($data["lifting"]) ? (int)$data["lifting"] : "",//升降序
                "field" => isset($data["field"]) ? $data["field"] : "",//对应字段
                "expert_id" => isset($data["expert_id"]) ? $data["expert_id"] : 0,//专家id
                "author_id" => isset($data["author_id"]) ? $data["author_id"] : 0,//作者id
                "name" => isset($data["name"]) ? $data["name"] : '',//专家名称
            ], (int)$data["type_id"], $status, $contains, $sys, $isSource);
            $likeService = new LikeService();
            if ($result["code"] == _MSG_SYSTEM_SUCCESS) {
                foreach ($result["data"]["list"] as $k => $v) {
                    //获取redis真实粉丝数与假的总和
                    $blog_likes_true = $likeService->getBlogLikeCount($v['blog_id'], $sys);
                    $result["data"]["list"][$k]["blog_likes"] = $blog_likes_true['count'];
                    $result["data"]["list"][$k]["blog_likes_total"] = $v['blog_likes_fake'] + $blog_likes_true['count'];
                    $result["data"]["list"][$k]["home_page_show"] = 0;
                    if (isset($blogs[$v["blog_id"]])) {
                        $result["data"]["list"][$k]["home_page_show"] = 1;
                    }

                    $thumbnail_url = json_decode($v['thumbnail_url'], true);
                    if (is_array($thumbnail_url)) {
                        $thumbnail_url = $thumbnail_url[0];
                    }
                    $thumbnail_url = $thumbnail_url ?: $v['thumbnail_url'];
                    $result["data"]["list"][$k]['thumbnail_url'] = $thumbnail_url;
                    $result["data"]["list"][$k]['publish_at_display'] = date("Y-m-d H:i:s", $v["created_at"]);
                }
            }

        } elseif ((int)$data["type_id"] == 2) {
            $kj = Lottery::getInstance()->getLatestQG();
            if ($kj["meta"]["code"] == _MSG_SUCCESS) {
                $kj = $kj["response"]["list"];
            } else {
                $kj = [];
            }

            $kjList = [];
            foreach ($kj as $item) {
                $kjList[$item["lottery_id"]] = $item["lottery_no"];
            }

            $blogs = Redis::getInstance()->redisHgetAll(_HOME_PAGE_BLOG . "list");
            $contains = [];
            if ($is_home_show) {
                if ($blogs['code'] == _MSG_SYSTEM_SUCCESS) {
                    foreach ($blogs['data'] as $blog) {
                        $blog = unserialize($blog);
                        if (isset($blog['blog_id']) && !in_array($blog['blog_id'], $contains)) {
                            $contains[] = $blog['blog_id'];
                        }
                    }
                }
            }

            $result = BlogInstanceModel::getInstance()->selectType2ListByPager([
                "page_no" => isset($data["page_no"]) ? (int)$data["page_no"] : 1,
                "page_count" => isset($data["page_count"]) ? (int)$data["page_count"] : 10,
                "search" => isset($data["search"]) ? $data["search"] : "",
                "search_user" => isset($data["search_user"]) ? $data["search_user"] : "",
                "order" => isset($data["order"]) ? (int)$data["order"] : 0,
                "lottery_id" => isset($data["lottery_id"]) ? $data["lottery_id"] : "",
                "lottery_no" => isset($data["lottery_no"]) ? $data["lottery_no"] : "",
                "expose" => isset($data["expose"]) ? (int)$data["expose"] : 0,
                "lifting" => isset($data["lifting"]) ? (int)$data["lifting"] : "",//升降序
                "field" => isset($data["field"]) ? $data["field"] : "",//对应字段
                "expert_id" => isset($data["expert_id"]) ? $data["expert_id"] : 0,//专家id
                "name" => isset($data["name"]) ? $data["name"] : '',//专家名称
            ], $status, $kjList, $contains, $sys);
            if ($result["code"] == _MSG_SYSTEM_SUCCESS) {
                foreach ($result["data"]["list"] as $k => $v) {
                    $result["data"]["list"][$k]["home_page_show"] = 0;
                    if (isset($blogs["data"][$v["blog_id"]])) {
                        $result["data"]["list"][$k]["home_page_show"] = 1;
                    }
                }
            }
        } else {
            $result = ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => "type_id 不存在", "data" => []];
        }

        return $result;
    }


    /**
     * @description 上下篇
     */
    public function adjoinTwo($blogId, $type, $lotteryId, $LotteryNo)
    {
        if ($type == 1) {
            $itemPre = DB::table("blog_instance")
                ->field("blog_id, title")
                ->where("type_id", $type)
                ->where("status", 1)
                ->where("blog_id", ">", $blogId)
                ->order("blog_id", "asc")
                ->find();

            $itemNext = DB::table("blog_instance")
                ->field("blog_id, title")
                ->where("type_id", $type)
                ->where("status", 1)
                ->where("blog_id", "<", $blogId)
                ->order("blog_id", "desc")
                ->find();
        }

        if ($type == 2) {
            $itemPre = DB::table("blog_instance")
                ->field("blog_id, title")
                ->where("type_id", $type)
                ->where("lottery_id", $lotteryId)
                ->where("lottery_no", $LotteryNo)
                ->where("status", 1)
                ->where("blog_id", ">", $blogId)
                ->order("blog_id", "asc")
                ->find();

            $itemNext = DB::table("blog_instance")
                ->field("blog_id, title")
                ->where("type_id", $type)
                ->where("status", 1)
                ->where("blog_id", "<", $blogId)
                ->where("lottery_id", $lotteryId)
                ->order("blog_id", "desc")
                ->find();

            if (empty($itemPre)) {
                $itemPre = DB::table("blog_instance")
                    ->field("blog_id, title")
                    ->where("type_id", $type)
                    ->where("lottery_id", $lotteryId)
                    ->where("lottery_no", ">", $LotteryNo)
                    ->where("status", 1)
                    ->where("blog_id", ">", $blogId)
                    ->order("blog_id", "asc")
                    ->find();
            }

            if (empty($itemNext)) {
                $itemNext = DB::table("blog_instance")
                    ->field("blog_id, title")
                    ->where("type_id", $type)
                    ->where("lottery_id", $lotteryId)
                    ->where("lottery_no", "<", $LotteryNo)
                    ->where("status", 1)
                    ->where("blog_id", "<", $blogId)
                    ->order("blog_id", "desc")
                    ->find();
            }
        }

        if (empty($itemPre)) {
            $itemPre = [
                "blog_id" => 0,
                "title" => "没有了"
            ];
        }

        if (empty($itemNext)) {
            $itemNext = [
                "blog_id" => 0,
                "title" => "没有了"
            ];
        }

        return [$itemPre, $itemNext];
    }

    /**
     * @description 上下篇 + 已发布
     */
    public function adjoinTwoPublished($blogId, $type, $lotteryId, $LotteryNo, $sys = 343)
    {
        if ($type == 1) {
            $itemPre = DB::table("blog_instance")
                ->field("blog_id, title")
                ->where("type_id", $type)
                ->where("status", 1)
                ->where("blog_id", ">", $blogId)
                ->where("publish_at", "<=", time())
                ->where("sys", $sys)
                ->order("blog_id", "asc")
                ->find();

            $itemNext = DB::table("blog_instance")
                ->field("blog_id, title")
                ->where("type_id", $type)
                ->where("status", 1)
                ->where("blog_id", "<", $blogId)
                ->where("publish_at", "<=", time())
                ->where("sys", $sys)
                ->order("blog_id", "desc")
                ->find();
        }

        if ($type == 2) {
            $itemPre = DB::table("blog_instance")
                ->field("blog_id, title")
                ->where("type_id", $type)
                ->where("lottery_id", $lotteryId)
                ->where("lottery_no", $LotteryNo)
                ->where("status", 1)
                ->where("blog_id", ">", $blogId)
                ->where("publish_at", "<=", time())
                ->where("sys", $sys)
                ->order("blog_id", "asc")
                ->find();

            $itemNext = DB::table("blog_instance")
                ->field("blog_id, title")
                ->where("type_id", $type)
                ->where("status", 1)
                ->where("blog_id", "<", $blogId)
                ->where("lottery_id", $lotteryId)
                ->where("publish_at", "<=", time())
                ->where("sys", $sys)
                ->order("blog_id", "desc")
                ->find();

            if (empty($itemPre)) {
                $itemPre = DB::table("blog_instance")
                    ->field("blog_id, title")
                    ->where("type_id", $type)
                    ->where("lottery_id", $lotteryId)
                    ->where("lottery_no", ">", $LotteryNo)
                    ->where("status", 1)
                    ->where("blog_id", ">", $blogId)
                    ->where("publish_at", "<=", time())
                    ->where("sys", $sys)
                    ->order("blog_id", "asc")
                    ->find();
            }

            if (empty($itemNext)) {
                $itemNext = DB::table("blog_instance")
                    ->field("blog_id, title")
                    ->where("type_id", $type)
                    ->where("lottery_id", $lotteryId)
                    ->where("lottery_no", "<", $LotteryNo)
                    ->where("status", 1)
                    ->where("blog_id", "<", $blogId)
                    ->where("publish_at", "<=", time())
                    ->where("sys", $sys)
                    ->order("blog_id", "desc")
                    ->find();
            }
        }

        if (empty($itemPre)) {
            $itemPre = [
                "blog_id" => 0,
                "title" => "没有了"
            ];
        }

        if (empty($itemNext)) {
            $itemNext = [
                "blog_id" => 0,
                "title" => "没有了"
            ];
        }

        return [$itemPre, $itemNext];
    }

    /**
     * 获取单篇详情
     */
    public function getBlogInfoById($sys, $blogId, $typeId)
    {
        return BlogInstanceModel::getInstance()->getBlogInfoById($sys, $blogId, $typeId);
    }

    /**
     * 批量获取多篇详情
     */
    public function getBlogInfoByIdArr($sys, $blogIdArr, $typeId)
    {
        return BlogInstanceModel::getInstance()->getBlogInfoByIdArr($sys, $blogIdArr, $typeId);
    }

    /**
     * 获取关注的作者文章列表
     */
    public function getMyFollowAuthorArticleList($userId, $sys, $page = 1, $pageCount = 10)
    {
        //获取关注的作者
        $service = new FollowService();
        $authorIdArr = $service->getMyLikeAuthorIdList($userId, $sys);

        //获取文章列表
        $blogModel = new BlogModel();
        $blogInfo = $blogModel->getBlogInstanceByAuthorId($authorIdArr, BlogModel::TYPE_AUTHOR_ARTICLE, $sys, $page, $pageCount);
        //判断是否还有
        $hasMore = $blogModel->getBlogInstanceByAuthorId($authorIdArr, BlogModel::TYPE_AUTHOR_ARTICLE, $sys, $page + 1, $pageCount);

        //获取文章作者
        $authorService = new AuthorService();
        $authorIdArr = array_column($blogInfo, 'author_id');
        $authorInfo = $authorService->getAuthorInfoByIdArr($authorIdArr, $sys);
        $likeService = new LikeService();

        $data = [];
        foreach ($blogInfo as $value) {
            //获取文章点赞数量
            $likeCount = $likeService->getBlogLikeCount($value['blog_id'], $sys);
            $likeCount = $likeCount['count'];
            $data[] = [
                'title' => $value['title'],
                'img' => $value['thumbnail_url'],
                'min' => replaceUrl($value['thumbnail_url'], 0),
                'author_name' => isset($authorInfo[$value['author_id']]) ? $authorInfo[$value['author_id']]['author_name'] : '',
                'create_date' => DataService::getInstance()->_handleCreateTimeTwo(date('Y-m-d H:i:s', $value['created_at'])),
                'like' => $likeCount,
                'like_total' => $value['blog_likes_fake'] + $likeCount,
                'blog_id' => $value['blog_id']
            ];
        }

        return [
            'data' => $data,
            'has_more' => empty($hasMore) ? 0 : 1
        ];
    }


}