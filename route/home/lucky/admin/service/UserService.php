<?php
/**
 * 用户
 */

namespace app\lucky\admin\service;


use app\hook\adminLog\context\AdminLogContext;
use app\hook\adminLog\controller\AdminLog;
use app\sports\certification\model\CertificationModel;
use app\lucky\common\InstanceTrait;
use app\lucky\pay\service\UserGoldService;
use app\lucky\user\model\UserV2Model;
use app\sports\disablesendmsg\model\DisablesendmsgModel;
use app\lucky\topic\service\TopicService;
use app\lucky\comment\service\CommentService;

class UserService
{
    use InstanceTrait;

    //表名
    const ADMIN_OP_TABLE_NAME = 'user';

    /*
     * 用户封禁列表
     */
    public function getForbidList($data, $sys = 343)
    {
        $page = isset($data['page_no']) ? $data['page_no'] : 1;
        $pageCount = isset($data['page_count']) ? $data['page_count'] : 10;
        $nickname = isset($data['nickname']) ? $data['nickname'] : '';
        $phone = isset($data['phone']) ? $data['phone'] : '';
        if ($nickname && $phone) {
            $userIdArr = \app\lucky\user\service\UserService::getInstance()->getUserInfoByLikeUserNicknameAndPhone($nickname, $phone, $sys);
        } else if ($nickname) {
            $userIdArr = \app\lucky\user\service\UserService::getInstance()->getUserInfoByLikeUserNickname($nickname, $sys);
        } else if ($phone) {
            $userIdArr = \app\lucky\user\service\UserService::getInstance()->getUserInfoByLikeUserPhone($phone, $sys);
        } else {
            $userIdArr = [0];
        }

        $result = UserV2Model::getInstance()->selectStatusListByPager($page, $pageCount, $userIdArr, $sys, '', 1);
        $data = $result['data'];

        //处理前端数据格式
        if ($data) {
            foreach ($data as $k => $v) {
                $data[$k]['deleted_at'] = date('m-d H:i:s', $v['deleted_at']);
            }
        }

        $pageTotal = (int)ceil($result['total'] / $pageCount);
        $page_info = [
            'total' => $result['total'],//总记录
            'page_no' => $page,//页数
            'page_count' => $pageCount,//每页显示数量
            'page_total' => $pageTotal,//总页数
        ];

        return [
            'page_info' => $page_info,
            'list' => $data,
        ];
    }

    /**
     * 用户封禁/解除
     *
     */
    public function forbidUpdate($userId, $data, $sys = 343)
    {
        $update = [
            'deleted_at' => time(),
            'is_del' => $data['is_del'],
            'admin_id' => $data['admin_id'],
            "sys" => $sys
        ];


        //隐藏相关帖子与评论
        $topicService = new TopicService();
        $commentService = new CommentService();
        if ($data['is_del'] == 1) {
            //禁封
            $topicService->userTopicForbidUpdate($userId, $sys);
            $commentService->userCommentForbidUpdate($userId, $sys);
        } else {
            //解封
            $topicService->userTopicUnfastenUpdate($userId, $sys);
            $commentService->userCommentUnfastenUpdate($userId, $sys);
        }


        if ($data['is_del']) {
            $opInfo = '用户封禁';
        } else {
            $opInfo = '用户封禁解除';
        }

        if (isset($data['delete_reason'])) {
            $update['delete_reason'] = $data['delete_reason'];
        }

        $result = UserV2Model::getInstance()->forbidUpdate($userId, $update, $sys);

        if ($result) {
            //添加操作日志
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_DELETE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $userId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "操作成功", "data" => []];
        } else {
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "操作失败", "data" => []];
        }
    }

    /**
     * 信息
     * @return array 返回类型
     */
    public function getInfo($id, $sys = 343)
    {
        //用户信息
        $userInfo = UserV2Model::getInstance()->getOneById($id, $sys);
        if (!$userInfo) {
            return ["code" => _MSG_NOT_FOUND, "msg" => "查询失败", "data" => []];
        }

        //禁言状态
        $disablesendmsgModel = new DisablesendmsgModel();
        $disablesendmsgInfo = $disablesendmsgModel->getOneByUserId($id, $sys);
        $userInfo['is_jiechu'] = $disablesendmsgInfo['is_jiechu'] == 1 ? '正常' : '禁言';
        $userInfo['is_del'] = $userInfo['is_del'] == 1 ? '禁封' : '正常';
        $userInfo['login_mode'] = $userInfo['login_mode'] == 'tel' ? '手机' : '微信';

        //金豆,等级，经验值信息
        $userGoldAndLevelInfo = UserGoldService::getInstance()->getUserGoldAndLevel($id, $sys);
        //金豆数量
        $userInfo['gold_num'] = isset($userGoldAndLevelInfo['num']) ? $userGoldAndLevelInfo['num'] : 0;
        //等级
        $userInfo['level'] = 'Lv.' . (isset($userGoldAndLevelInfo['level']) ? $userGoldAndLevelInfo['level'] : 1);

        //帖子数量
        $userInfo['topic_sum'] = \app\lucky\user\service\UserService::getInstance()->GetUserTopicTotal($id, $sys);

        //实名认证信息
        $certificationInfo = CertificationModel::getInstance()->getOneByUserId($id, $sys);

        //处理前端数据格式
        $userInfo['name'] = $certificationInfo['name'];
        $userInfo['sex'] = $certificationInfo['sex'] == 1 ? '男' : '女';
        $userInfo['certificate_num'] = $certificationInfo['certificate_num'];
        $userInfo['certificate_imgurl1'] = $certificationInfo['certificate_imgurl1'];
        $userInfo['certificate_imgurl2'] = $certificationInfo['certificate_imgurl2'];
        $userInfo['certificate_imgurl3'] = $certificationInfo['certificate_imgurl3'];
        $userInfo['certificate_name'] = '身份证';
        $userInfo['is_admin'] = $userInfo['is_admin'] == 1 ? '管理员' : '普通用户';
        switch ($certificationInfo['audit_flag']) {
            case 2:
                $userInfo['auditFlagName'] = '已通过';
                break;
            case 3:
                $userInfo['auditFlagName'] = '未通过';
                break;
            default:
                $userInfo['auditFlagName'] = '待审核';
        }

        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "查询成功", "data" => $userInfo];
    }

    /**
     * 设置为管理员
     */
    public function setAdminByUserId($userId, $isAdmin, $sys)
    {
        $data = [
            "is_admin" => $isAdmin,
            "updated_at" => date('Y-m-d H:i:s')
        ];

        if ($isAdmin) {
            $opInfo = '设为管理员';
        } else {
            $opInfo = '取消管理员';
        }

        $result = UserV2Model::getInstance()->updateUser($userId, $data, $sys);

        if ($result['code'] == _MSG_SYSTEM_SUCCESS) {
            //添加操作日志
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_UPDATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $userId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return true;
        } else {
            return false;
        }

    }


}
