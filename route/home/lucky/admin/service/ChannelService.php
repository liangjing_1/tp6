<?php
/**
 * 频道
 */

namespace app\lucky\admin\service;

use app\hook\adminLog\context\AdminLogContext;
use app\hook\adminLog\controller\AdminLog;
use app\lucky\blog\model\BlogModel;
use app\lucky\channel\model\ChannelModel;
use app\lucky\blog\model\BlogInstanceModel;
use think\Db;

class ChannelService
{
    //表名
    const ADMIN_OP_TABLE_NAME = 'channel';

    /*
     * 频道的列表
     */
    public function getList($data, $sys = 343)
    {
        $page = isset($data['page_no']) ? $data['page_no'] : 1;
        $pageCount = isset($data['page_count']) ? $data['page_count'] : 10;
        $search = isset($data['name']) ? $data['name'] : '';

        $result = ChannelModel::getInstance()->selectStatusListByPager($page, $pageCount, $search, $sys);
        $data = $result['data'];

        //处理前端数据格式
        if ($data) {
            foreach ($data as $k => $v) {
                $data[$k]['create_time'] = date('m-d H:i:s');
                //资讯数量
                $data[$k]['blog_total'] = BlogInstanceModel::getInstance()->getCountBlogByChannelId($v['id'], $sys);
            }
        }

        $pageTotal = (int)ceil($result['total'] / $pageCount);
        $page_info = [
            'total' => $result['total'],//总记录
            'page_no' => $page,//页数
            'page_count' => $pageCount,//每页显示数量
            'page_total' => $pageTotal,//总页数
        ];

        return [
            'page_info' => $page_info,
            'list' => $data,
        ];
    }

    /**
     * 添加
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function create($data, $sys = 343)
    {
        //数据是否存在
        $channel = ChannelModel::getInstance()->getOneByName($data['name'], $sys);
        if (isset($channel['name'])) {
            return ["code" => _MSG_AlREADY_EXIST, "msg" => "名字不能重复", "data" => []];
        }

        $data = [
            "name" => $data['name'],
            "create_time" => time(),
            "sys" => $sys
        ];

        $result = ChannelModel::getInstance()->insert($data);

        if ($result) {
            //添加操作日志
            $opInfo = '添加频道';
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_CREATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $result, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "添加成功", "data" => []];
        } else {
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "添加失败", "data" => []];
        }
    }

    /**
     * 修改
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function update($channelId, $data, $sys = 343)
    {
        //名字不能重复
        $channel = ChannelModel::getInstance()->getOneById($data['channel_id'], $sys);
        if (!$channel) {
            return ["code" => _MSG_NOT_FOUND, "msg" => "数据不存在", "data" => []];
        }

        //名字不能重复
        $channelOldInfo = ChannelModel::getInstance()->getOneByNameAndNotInById($data['channel_id'], $data['name'], $sys);
        if (isset($channelOldInfo['name'])) {
            return ["code" => _MSG_AlREADY_EXIST, "msg" => "名字不能重复", "data" => []];
        }

        $opInfo = '修改频道信息';
        $update = [
            'update_time' => time(),
            'name' => $data['name'],
            "sys" => $sys
        ];

        $result = ChannelModel::getInstance()->update($channelId, $update, $sys);

        if ($result) {
            //添加操作日志
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_UPDATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $channelId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "操作成功", "data" => []];
        } else {
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "操作失败", "data" => []];
        }
    }

    /**
     * 修改
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function updateIsShow($channelId, $isShow, $sys = 343)
    {
        //数据是否存在
        $channel = ChannelModel::getInstance()->getOneById($channelId, $sys);
        if (!$channel) {
            return ["code" => _MSG_NOT_FOUND, "msg" => "数据不存在", "data" => []];
        }

        if ($isShow) {
            $opInfo = '开启频道';
        } else {
            $opInfo = '关闭频道';
        }

        $update = [
            'update_time' => time(),
            'is_show' => $isShow,
            "sys" => $sys
        ];

        $result = ChannelModel::getInstance()->update($channelId, $update, $sys);

        if ($result) {
            //添加操作日志
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_UPDATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $channelId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "操作成功", "data" => []];
        } else {
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "操作失败", "data" => []];
        }
    }

    /**
     * 删除
     */
    public function delete($channelId, $sys = 343)
    {
        // 启动事务
        Db::startTrans();
        try {
            $result = ChannelModel::getInstance()->delete($channelId, $sys);
            if ($result) {
                //添加操作日志
                $opInfo = '删除频道';
                $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_DELETE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $channelId, $opInfo);
                \think\facade\Hook::listen('admin_log', $context);
            } else {
                Db::rollback();
                return false;
            }

            $blogModel = new BlogModel();
            $result = $blogModel->deleteBlogsByChannelId($channelId, $blogModel::TYPE_AUTHOR_ARTICLE, $sys);
            if (!$result) {
                Db::rollback();
                return false;
            }

            // 提交事务
            Db::commit();
            return true;
        } catch (\Exception $e) {
            \Log::error('删除频道:' . $e->getMessage());
            // 回滚事务
            Db::rollback();
            return false;
        }
    }


}