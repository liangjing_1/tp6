<?php
/**
 * 公告　
 */

namespace app\lucky\admin\service;

use app\hook\adminLog\context\AdminLogContext;
use app\hook\adminLog\controller\AdminLog;
use app\lucky\notice\model\NoticeModel;

class NoticeService
{
    //表名
    const ADMIN_OP_TABLE_NAME = 'notice';

    public $model = '';

    public function __construct()
    {
        $this->model = new NoticeModel();
    }


    /**
     * 查询列表
     * @access public
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function getList($data, $sys = 343)
    {
        $page = isset($data['page_no']) ? $data['page_no'] : 1;
        $pageCount = isset($data['page_count']) ? $data['page_count'] : 10;
        $search = isset($data['search']) ? $data['search'] : '';
        $statusArr = [1, 2];

        $result = $this->model->selectStatusListByPager($page, $pageCount, $search, $sys, $statusArr);

        $data = $result['data'];
        //处理前端数据格式
        if ($data) {
            foreach ($data as $k => $v) {
                if ($v['updated_at']) {
                    $data[$k]['created_time'] = date('Y-m-d H:i:s', $v['updated_at']);
                } else {
                    $data[$k]['created_time'] = date('Y-m-d H:i:s', $v['created_at']);
                }
            }
        }

        $pageTotal = (int)ceil($result['total'] / $pageCount);
        $page_info = [
            'total' => $result['total'],//总记录
            'page_no' => $page,//页数
            'page_count' => $pageCount,//每页显示数量
            'page_total' => $pageTotal,//总页数
        ];

        return [
            'page_info' => $page_info,
            'list' => $data,
        ];
    }


    /**
     * 添加公告
     * @access public
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function addNotice($data, $sys = 343)
    {
        //防止重复添加
        $noticeData = $this->model->getOneByTitle($data['title'], $sys);
        if ($noticeData) {
            return ["code" => _MSG_AlREADY_EXIST, "msg" => "该标题已存在", "data" => []];
        }

        $data = [
            "admin_id" => $data['admin_id'],
            "title" => $data['title'],
            "content" => $data['content'],
            "sys" => $sys
        ];

        $result = $this->model->insert($data, $sys);

        if ($result) {
            //添加操作日志
            $opInfo = '添加公告';
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_CREATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $result, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "添加成功", "data" => []];
        } else {
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "添加失败", "data" => []];
        }
    }

    /**
     * 修改公告
     * @access public
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function editNotice($noticeId, $data, $sys = 343)
    {
        //防止重复添加
        if (isset($data['title']) && $data['title']) {
            $noticeData = $this->model->getOneByTitle($data['title'], $sys);
            if ($noticeData) {
                return ["code" => _MSG_AlREADY_EXIST, "msg" => "该标题已存在", "data" => []];
            }
            $update['title'] = $data['title'];
        }

        $update = [
            'updated_at' => time(),
            "sys" => $sys
        ];

        if (isset($data['content'])) {
            $update['content'] = $data['content'];
        }
        if (isset($data['status'])) {
            $update['status'] = $data['status'];
        }

        $result = $this->model->update($noticeId, $update, $sys);

        if ($result) {
            //添加操作日志
            $opInfo = '修改公告';
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_UPDATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $noticeId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "操作成功", "data" => []];
        } else {
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "操作成功", "data" => []];
        }
    }

    /**
     * 删除公告状态
     * @access public
     * @param int $noticeId 公告id
     * @param int $status 公告状态
     * @param string $sys 系统
     * @return bool 返回类型
     */
    public function delete($noticeId, $status, $sys = 343)
    {
        $noticeInfo = $this->model->getOneById($noticeId, $sys);
        if (!$noticeInfo) {
            return false;
        }

        $delete = $this->model->delete($noticeId, $status, $sys);

        if ($delete){
            //添加操作日志
            $opInfo = '删除公告';
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_DELETE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $noticeId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);
        }

        return $delete;
    }


}