<?php
/**
 * 作者
 */

namespace app\lucky\admin\service;

use app\common\DataService;
use app\hook\adminLog\context\AdminLogContext;
use app\hook\adminLog\controller\AdminLog;
use app\lucky\author\model\AuthorModel;
use app\lucky\blog\model\BlogInstanceModel;
use app\lucky\blog\model\BlogModel;
use app\lucky\channel\model\ChannelModel;
use app\lucky\common\OSS;
use app\lucky\follow\service\FollowService;
use app\lucky\like\service\LikeService;

class AuthorService
{
    //表名
    const ADMIN_OP_TABLE_NAME = 'author';

    /**
     * 作者时长数组
     * @return array
     */
    public function getauthorDuration()
    {
        $result = [
            '1天',
            '7天',
            '15天',
            '30天',
            '1年',
            '永久',
        ];
        return $result;

    }

    /**
     * 根据ID获取作者信息
     */
    public function getAuthorInfoByIdArr($idArr, $sys)
    {
        return AuthorModel::getInstance()->getListByAuthorIdArr($idArr, $sys);
    }

    /*
     * 作者的列表
     */
    public function getList($data, $sys = 343)
    {
        $page = isset($data['page_no']) ? $data['page_no'] : 1;
        $pageCount = isset($data['page_count']) ? $data['page_count'] : 10;
        $search = isset($data['nickname']) ? $data['nickname'] : '';

        $result = AuthorModel::getInstance()->selectStatusListByPager($page, $pageCount, $search, $sys);
        $data = $result['data'];
        $followService = new FollowService;
        $blogModel = new BlogModel();

        //处理前端数据格式
        if ($data) {
            foreach ($data as $k => $v) {
                $trueFollowCount = $followService->getAuthorFollowCount($v['id'], $sys);
                $data[$k]['true_fan'] = $trueFollowCount['count'];
                $data[$k]['virtual_true_total'] = $v['virtual_fan'] + $trueFollowCount['count'];
                //发帖总数
                $data[$k]['blog_total'] = $blogModel->getBlogCountByAuthorId($v['id'],4,$sys);
            }
        }

        $pageTotal = (int)ceil($result['total'] / $pageCount);
        $page_info = [
            'total' => $result['total'],//总记录
            'page_no' => $page,//页数
            'page_count' => $pageCount,//每页显示数量
            'page_total' => $pageTotal,//总页数
        ];

        return [
            'page_info' => $page_info,
            'list' => $data,
        ];
    }

    /**
     * 添加
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function create($data, $sys = 343)
    {
        $data = [
            "admin_id" => $data['admin_id'],
            "author_name" => $data['author_name'],
            "author_description" => isset($data['author_description']) ? $data['author_description'] : '',
            "virtual_fan" => isset($data['virtual_fan']) ? $data['virtual_fan'] : '',
            "author_headimgurl" => isset($data['author_headimgurl']) ? $data['author_headimgurl'] : '',
            "created_time" => time(),
            "sys" => $sys
        ];

        $result = AuthorModel::getInstance()->insert($data);

        if ($result) {
            //添加操作日志
            $opInfo = '添加作者';
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_CREATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $result, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "添加成功", "data" => []];
        } else {
            if (isset($data['author_headimgurl'])) {
                OSS::getInstance()->deleteFile($data["author_headimgurl"]);
            }
            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "添加失败", "data" => []];
        }
    }

    /**
     * 修改
     * @param array $data 数据数组
     * @param string $sys 系统
     * @return array 返回类型
     */
    public function update($authorId, $data, $sys = 343)
    {
        $opInfo = '修改作者信息';
        $update = [
            'updated_time' => time(),
            "sys" => $sys
        ];

        if (isset($data['author_name'])) {
            $update['author_name'] = $data['author_name'];
        }
        if (isset($data['virtual_fan'])) {
            $update['virtual_fan'] = $data['virtual_fan'];
        }
        if (isset($data['author_description'])) {
            $update['author_description'] = $data['author_description'];
        }
        if (isset($data['author_headimgurl'])) {
            $authorOldInfo = AuthorModel::getInstance()->getOneById($data['author_id'], $sys);
            if ($data['author_headimgurl'] != $authorOldInfo["author_headimgurl"]) {
                $update['author_headimgurl'] = $data['author_headimgurl'];
            }
        }

        $result = AuthorModel::getInstance()->update($authorId, $update, $sys);

        if ($result) {
            //更新图片删除旧图
            if (isset($update['author_headimgurl'])) {
                OSS::getInstance()->deleteFile($authorOldInfo["author_headimgurl"]);
            }
            //添加操作日志
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_UPDATE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $authorId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);

            return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "操作成功", "data" => []];
        } else {
            //更新图片删除新图
            if (isset($update['author_headimgurl'])) {
                OSS::getInstance()->deleteFile($data["author_headimgurl"]);
            }

            return ["code" => _MSG_NOT_ALLOWED_METHOD, "msg" => "操作失败", "data" => []];
        }
    }

    /**
     * 信息
     * @return array 返回类型
     */
    public function getInfo($authorId, $pageNo = 1, $pageCount = 10, $sys = 343)
    {
        //作者信息
        $authorInfo = AuthorModel::getInstance()->getOneById($authorId, $sys);
        if (!$authorInfo) {
            return ["code" => _MSG_NOT_FOUND, "msg" => "查询失败", "data" => []];
        }
        //作者粉丝
        $followService = new FollowService();
        $trueFollowCount = $followService->getAuthorFollowCount($authorId, $sys);
        $authorInfo['virtual_true_total'] = $authorInfo['virtual_fan'] + $trueFollowCount['count'];
        $authorInfo['true_fan'] = $trueFollowCount['count'];
        $data['authorInfo'] = $authorInfo;

        //获取资讯列表
        $blogList = BlogInstanceModel::getInstance()->getAuthorBlogListByPager($authorId, $pageNo, $pageCount, $sys);
        //处理前端数据格式
        $pageTotal = (int)ceil($blogList['total'] / $pageCount);
        $page_info = [
            'total' => $blogList['total'],//总记录
            'page_no' => $pageNo,//页数
            'page_count' => $pageCount,//每页显示数量
            'page_total' => $pageTotal,//总页数
        ];

        $data['page_info'] = $page_info;
        $data['blogList'] = $blogList['data'];

        if ($data['blogList']) {
            $likeService = new LikeService();
            foreach ($data['blogList'] as $k => $v) {
                //创建时间
                if ($v['updated_at']) {
                    $publish_at_display = date('Y-m-d H:i:s', $v['updated_at']);
                } else {
                    $publish_at_display = date('Y-m-d H:i:s', $v['created_at']);
                }
                $data['blogList'][$k]['publish_at_display'] = DataService::getInstance()->_handleCreateTimeTwo($publish_at_display);

                //真实点赞数
                $data['blogList'][$k]['blog_likes'] = $likeService->getBlogLikeCount($v['blog_id'], $sys)['count'];
                //虚拟与真实总和的点赞数
                $data['blogList'][$k]['blog_likes_total'] = $data['blogList'][$k]['blog_likes'] + $v['blog_likes_fake'];
                //频道信息
                $channelInfo = ChannelModel::getInstance()->getOneById($v['channel_id'], $sys);
                $data['blogList'][$k]['channel_name'] = isset($channelInfo['name']) ? $channelInfo['name'] : '';
            }
        }

        $data['authorInfo']['totalBlog'] = $blogList['total'];

        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "查询成功", "data" => $data];
    }

    /**
     * 删除
     */
    public function delete($authorId, $sys = 343)
    {
        $result = AuthorModel::getInstance()->delete($authorId, $sys);

        if ($result) {
            //添加操作日志
            $opInfo = '删除作者';
            $context = AdminLog::getInstance()->newAdminLogContext(AdminLogContext::ADMIN_OP_TYPE_DELETE, $this::ADMIN_OP_TABLE_NAME, AdminLogContext::ADMIN_OP_STATUS_SUCCESS, $authorId, $opInfo);
            \think\facade\Hook::listen('admin_log', $context);
        }

        return $result;
    }


}