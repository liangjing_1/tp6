<?php


namespace app\lucky\admin\service;

use app\lucky\admin\model\WxMenuModel;
use think\Exception;
use think\facade\Env;
use app\common\Redis;
use think\facade\Log;

class WxMenuService
{
    public $appid = '';
    public $appsecret = '';
    public $xmlobj;
    public $token;

    public function __construct()
    {
        $this->appid = Env::get('wx.appid');
        $this->appsecret = Env::get('wx.appsecret');
        $this->token = Env::get('wx.token');
    }

    /*
     * 获取access_token  全局有效
     * 缓存 redis
     */
    public function getAccessToken()
    {

        $key = 'lucky:access_token:wx';
        $data = Redis::getInstance()->redisGet($key);
        if ($data['code'] == _MSG_SYSTEM_SUCCESS) {
            return $data['data'];
        }

        // 缓存过期,重新获请求
        $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s';
        $url = sprintf($url, $this->appid, $this->appsecret);
        $access_token = json_decode(WxMenuService::request($url), true)['access_token'];

        Redis::getInstance()->redisSet($key, $access_token, 60 * 60 * 2);
        return $access_token;
    }


    public function noceStr(int $len = 16): string
    {
        $str = 'aAbBcCdDeEfhewkfeklwfnkkjlfhlekfekfjkfejw43iytfneskfhwiufnewnd';
        $str = str_shuffle($str);
        return substr($str, 0, $len);
    }

    // 获取当前URL
    public function currentUrl()
    {
        return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

    // 生成签名
    public function sign()
    {
        $jsapi_ticket = $this->jsapi_ticket();
        $noncestr = $this->noceStr();
        $timestamp = time();
        $url = $this->currentUrl();

        $str = 'jsapi_ticket=%s&noncestr=%s&timestamp=%s&url=%s';
        $str = sprintf($str, $jsapi_ticket, $noncestr, $timestamp, $url);


        $sign = sha1($str);

        return [
            'appId' => $this->appid,
            'timestamp' => $timestamp,
            'nonceStr' => $noncestr,
            'signature' => $sign,
            'url' => $url
        ];
    }


    /*
     *
     * 发送客户消息 ,先写好吧!顺便
     *
     */
    public function sendKefu(string $openid, string $msg)
    {
        $url = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=' . $this->getAccessToken();
        $data = '{
			"touser":"' . $openid . '",
			"msgtype":"text",
			"text":
			{
				"content":"' . $msg . '"
			}
		}';

        $json = json_decode(WxMenuService::request($url, $data), true);
        return $json;
    }

    /*
     *
     * 生成自定义菜单  异步的，如果想立刻马上能看到效果，则需要取消关注再关注
     */
    public function createMenu(string $data)
    {
        $url = 'https://api.weixin.qq.com/cgi-bin/menu/create?access_token=' . $this->getAccessToken();
        $json = json_decode(WxMenuService::request($url, $data), true);
        return $json;
    }

    /**
     * 删除创建好的自定义菜单
     */
    public function delMenu()
    {
        $url = 'https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=' . $this->getAccessToken();
        return json_decode(WxMenuService::request($url), true);
    }

    /*
     * 查询菜单
     */
    public function listMenu()
    {
        try {
            $keydata = 'lucky:wx:menu:query';
            $redis = Redis::getInstance()->redisGet($keydata);
            if ($redis['code'] == _MSG_SYSTEM_SUCCESS) {
                return $redis['data'];
            }
            $url = 'https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=' . $this->getAccessToken();
            //查询菜单
            $wxMenuModel = new WxMenuModel;
            $list = json_decode(WxMenuService::request($url), true)['selfmenu_info'];
            foreach ($list['button'] as $key => $v) {
                if (isset($v['sub_button']) && !empty($v['sub_button'])) {
                    foreach ($v['sub_button']['list'] as $k => $item) {
                        $list['button'][$key]['sub_button'][] = $item;
                        //将值查询出来追加进去
                        if (isset($item['type']) && $item['type'] == 'click') {
                            $click = explode('_', $item['key']);
                            $list['button'][$key]['sub_button'][$k]['key'] = $wxMenuModel->getText($click[1])['comment'];
                        }
                    }
                    unset($list['button'][$key]['sub_button']['list']);
                } else {
                    $list['button'][$key]['sub_button'] = [];
                }

                //将值查询出来追加进去
                if (isset($v['type']) && $v['type'] == 'click') {
                    $click = explode('_', $v['key']);
                    $list['button'][$key]['key'] = $wxMenuModel->getText($click[1])['comment'];
                }
            }
            Redis::getInstance()->redisSet($keydata, $list['button'], 60 * 60 * 3);
            return $list['button'];
        } catch (Exception $e) {
            Log::log('error', '获取公众号菜单列表错误');
        }

    }

    /*
     * 自定义封装curl函数
     */
    public static function request(string $url, $data = '', string $filepath = ''): string
    {


        if (!empty($filepath)) {

            $data['media'] = new \CURLFile($filepath);
        }

        $ch = \curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3780.0 Safari/537.36');

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        if (!empty($data)) {

            if (empty($filepath)) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json'
                ]);
            }
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        // 执行请求
        $data = curl_exec($ch);
        // 关闭
        curl_close($ch);
        return $data ?? '';
    }


    /*
     * 发送消息处理
     */
    public function acceptMsg()
    {

        try {
            $xml = file_get_contents('php://input');

            // 把xml记录到日志
            $this->writeLog($xml);


            $this->xmlobj = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);

//        $type = (string)$this->xmlobj->MsgType;
            $type = property_exists($this->xmlobj, 'MsgType') ? (string)$this->xmlobj->MsgType : 'default';
            $fun = $type . 'Fun';
            $arr = ['eventFun', 'subscribe', 'textFun', 'unsubscribe', 'click'];

            if (in_array($fun, $arr)) {
                return $this->$fun();
            } else {
                return 'success';
            }
        } catch (Exception $e) {
            return 'success';
        }
    }


    /*
     *发送图片
     */

    public function createImageXml(string $media_id)
    {
        $xml = '<xml>
				  <ToUserName><![CDATA[%s]]></ToUserName>
				  <FromUserName><![CDATA[%s]]></FromUserName>
				  <CreateTime>%s</CreateTime>
				  <MsgType><![CDATA[image]]></MsgType>
				  <Image>
				    <MediaId><![CDATA[%s]]></MediaId>
				  </Image>
				</xml>';
        return sprintf($xml, $this->xmlobj->FromUserName, $this->xmlobj->ToUserName, time(), $media_id);
    }


    /*
     *
     * 写日志
     */
    public function writeLog(string $data)
    {

        $log = "接受微信公众号的消息:--------------------------------";
        $log .= $data;
        $log .= '---------------------------------------------------';
        Log::info($log);
    }

    // 事件方法
    public function eventFun()
    {
        $event = strtolower($this->xmlobj->Event);
        if ($event == 'click') {
            return $this->$event();
        }
    }


    // 事件处理
    public function click()
    {
        $wxMenuModel = new WxMenuModel;
        $eventKey = (string)$this->xmlobj->EventKey;
        $click = explode('_', $eventKey);
        $res = $wxMenuModel->getText($click[1]);
        $keys = 'lucky:wx:menu:images';
        if ($res['comment'] == 'images') {
            $imagesMediaId = Redis::getInstance()->redisGet($keys);
            if ($imagesMediaId['code'] == _MSG_SYSTEM_SUCCESS) {
                return $this->createImageXml($imagesMediaId['data']);
            } else {
                $file = __DIR__ . '/0.jpeg';
                $resImage = $this->upFile('image', $file, 1)['media_id'];
                Redis::getInstance()->redisSet($keys, $resImage, 60 * 60 * 60 * 365);
                return $this->createImageXml($resImage);
            }
        } else {
            return $this->createTextXml($res['comment']);
        }

    }


    // 生成文本的xml
    public function createTextXml(string $cnt)
    {
        $xml = '<xml>
		<ToUserName><![CDATA[%s]]></ToUserName>
		<FromUserName><![CDATA[%s]]></FromUserName>
		<CreateTime>%s</CreateTime>
		<MsgType><![CDATA[text]]></MsgType>
		<Content><![CDATA[%s]]></Content>
		</xml>';
        return sprintf($xml, $this->xmlobj->FromUserName, $this->xmlobj->ToUserName, time(), $cnt);
    }

    /*
     *
     * 第一次接入鉴权
     * @param $data 微信公众发送过来的数据
     */
    public function checkSignature($data)
    {

        $signature = $data["signature"];

        $timestamp = $data["timestamp"];

        $nonce = $data["nonce"];

        $token = $this->token;
        $tmpArr = [$token, $timestamp, $nonce];

        sort($tmpArr, SORT_STRING);

        $tmpStr = implode($tmpArr);

        $tmpStr = sha1($tmpStr);

        if ($tmpStr == $signature) {
            return $data['echostr'];
        } else {
            return '';
        }
    }


    /*
     * 创建微信公众号菜单
     */

    public function createWxMenu($data)
    {
//        try {
        $wxMenuModel = new WxMenuModel;
        $jsonData = json_decode($data, true);
        $arr['button'] = [];
        foreach ($jsonData as $key => &$value) {
            if (isset($value['type']) && $value['type'] == 'click') {
                $oneData = [
                    'comment' => $value['key'],
                    'create_time' => date('Y-m-d H:i:s'),
                    'update_time' => date('Y-m-d H:i:s'),
                ];
                $oneID = $wxMenuModel->insertText($oneData);
                $value['key'] = 'text_' . $oneID;
            }
            if (isset($value['sub_button']) && !empty($value['sub_button'])) {
                foreach ($value['sub_button'] as $k => &$v) {
                    if (isset($v['type']) && $v['type'] == 'click') {
                        $twoData = [
                            'comment' => $v['key'],
                            'create_time' => date('Y-m-d H:i:s'),
                            'update_time' => date('Y-m-d H:i:s'),
                        ];
                        $twoID = $wxMenuModel->insertText($twoData);
                        $v['key'] = 'text_' . $twoID;
                    }
                }
            }
            $arr['button'][] = $value;
        }
        unset($value);
        unset($v);
        $keydata = 'lucky:wx:menu:query';
        Redis::getInstance()->redisDel($keydata);
        return $this->createMenu(json_encode($arr, JSON_UNESCAPED_UNICODE));
//        } catch (Exception $e) {
//            Log::error('创建微信公众号菜单错误!');
//        }
    }


    /*
     * 创建素材
     * @param $type （image）、语音（voice）、视频（video）和缩略图（thumb，主要用于视频与音乐格式的缩略图）
     * @param $filepah 文件路径
     * @forever 创建素材长久  临时素材0   永久素材1
     */

    public function upFile(string $type, string $filepath, int $forever = 0)
    {
        if ($forever == 0) {
            $url = 'https://api.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s';
        } else {
            $url = 'https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=%s&type=%s';
        }
        $url = sprintf($url, $this->getAccessToken(), $type);
        // 发起请求 并上传素材到公众号
        $json = WxMenuService::request($url, [], $filepath);
        return json_decode($json, true);
    }

    // 关注事件处理
    public function subscribe()
    {
        $text = "
            欢迎来到343部落，一个供彩友交流的平台[玫瑰]
            在这里你可以每天免费浏览：
            •福彩3D、双色球、大乐透、排三专家分析预测
            •福彩3D、双色球、大乐透、排三单挑一注
            •试机号预测
            如果有什么建议也欢迎后台给小编留言哦~
            
            查看定胆杀号，后台回复【天天金码】立即获取！
        ";

        return $this->createTextXml($text);
    }


    /*
     * 获取公众号素材列表
     * @param $type 图片（image）、视频（video）、语音 （voice）、图文（news）
     * @param $offset 从全部素材的该偏移位置开始返回，0表示从第一个素材 返回
     * @param 返回素材的数量，取值在1到20之间
     */

    public function getWxMaterialList($type, $offset, $count)
    {
        $data = [
            'type' => $type,
            'offset' => $offset,
            'count' => $count,
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=" . $this->getAccessToken();
        $json = WxMenuService::request($url, $data);
        return json_decode($json, true);
    }

    /*
     * 获取公众号素材的总数
     */

    public function getCountMaterialList()
    {
        $url = " https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token=" . $this->getAccessToken();
        $json = WxMenuService::request($url, []);
        return json_decode($json, true);
    }

    /*
     * 删除永久素材
     */

    public function deleteMaterial($media_id)
    {
        $url = " https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token=" . $this->getAccessToken();
        $data = [
            'media_id' => $media_id
        ];
        $json = WxMenuService::request($url, $data);
        return json_decode($json, true);
    }


    /*
     * 默认用户发送消息过来
     */
    public function textFun()
    {
        return 'success';
    }

    /*
     * 取消关注事件
     *
     */

    public function unsubscribe()
    {
        return 'success';
    }

    /*
     * 文章被点击时的事件
     */
    public function view()
    {
        return 'success';
    }

    /*
     * 默认
     */

    public function defaultFun()
    {
        return 'success';
    }
}