<?php
namespace app\lucky\admin\validate;

use app\lucky\common\InstanceTrait;
use think\Validate;

class UserValidate
{
    use InstanceTrait;

    public function checkForbidUpdate($data)
    {
        foreach ($data as $key => $value) {
            if (!is_array($value)) {
                $data[$key] = htmlentities($value);
            }
        }

        $rule = [
            "user_id" => "require|number",
            "delete_reason" => "max:200",
            "is_del" => "require|number|between:0,1"
        ];
        $msg = [
            "user_id.require" => "用户ID必须提供",
            "user_id.number" => "用户ID必须是数字",
            "delete_reason.max" => "封禁原因不得超过50字",
            "is_del.require" => "封禁状态必填",
            "is_del.number" => "封禁状态必须是数字",
            "is_del.between" => "封禁状态必须在（0解封-1封禁）之间"
        ];

        $validate = Validate::make($rule, $msg);
        $result = $validate->check($data);

        if (!$result) {
            return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => $validate->getError()];
        }
        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "校验成功", "data" => $data];
    }

}