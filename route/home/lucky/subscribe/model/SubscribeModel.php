<?php


namespace app\lucky\subscribe\model;

use think\Db;
use think\Model;

class SubscribeModel extends Model
{
    const NORMAL = 1;       //正常
    const DELETE = 2;       //删除

    /*
     * 查询用户订阅的专家号
     */
    public function getUserSubscribe($page_no, $page_count, $userId, $sys)
    {
        $list = Db::table('subscribe_list')
            ->where('sys', $sys)
            ->where('is_subscribe', 0)
            ->where('user_id', $userId);

        $data  = $list->page($page_no, $page_count)->order('is_click', 'aes')->select();
        $count = $list->count();

        return ['data' => $data, 'count' => $count];

    }

    /**
     * 根据专家号ID查询关注的用户ID
     * @param $expertArr
     * @param $sys
     */
    public function getUserByExpertUserId($expertUserId, $sys)
    {
        $list = Db::table('subscribe_list')->where('sys', $sys)->where('is_subscribe', 0)
            ->where('expert_id', $expertUserId)->select();

        return $list;
    }

    /*
     * 根据专家号查询相关专家信息
     */


    public function getWhereInExpertInfo($expertArr, $sys)
    {
        return Db::table('expert_user_two')
            ->where('sys', $sys)
            ->whereIn('id', $expertArr)
            ->where('status', self::NORMAL)
            ->select();
    }

    /*
     * 根据用户的ID与专家的ID更改订阅掌球的状态
     */

    public function updateWhereUserIdAndExpertIdSubscribe($userId, $expertId, $data, $sys)
    {
        return Db::table('subscribe_list')
            ->where('sys', $sys)
            ->where('expert_id', $expertId)
            ->where('user_id', $userId)
            ->update($data);

    }

}