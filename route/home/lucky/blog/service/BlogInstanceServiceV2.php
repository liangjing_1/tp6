<?php
/**
 *资讯
 */

namespace app\lucky\blog\service;

use app\common\DataService;
use app\lucky\author\model\AuthorModel;
use app\lucky\author\service\AuthorService;
use app\lucky\blog\context\BlogV2ListContext;
use app\lucky\blog\model\BlogInstanceModel;
use app\lucky\blog\model\BlogModel;
use app\lucky\follow\service\FollowService;
use app\lucky\like\service\LikeService;
use Log;

class BlogInstanceServiceV2
{

    /*
     * 列表
     */
    public function getList(BlogV2ListContext $context, $sys = 343)
    {
        $context->page_no = isset($context->page_no) ? $context->page_no : 1;
        $context->page_count = isset($context->page_count) ? $context->page_count : 10;
        //推荐
        $context->is_recommend = isset($context->is_recommend) ? $context->is_recommend : '';
        //频道id
        $context->channel_id = isset($context->channel_id) ? $context->channel_id : 0;

        $result = BlogInstanceModel::getInstance()->getBlogListByPagerV2($context, $sys);

        //处理前端数据格式
        $likeService = new LikeService();
        $data = [];
        if ($result['data']) {
            foreach ($result['data'] as $k => $v) {
                $authorInfo = AuthorModel::getInstance()->getOneById($v['author_id'], $sys);
                $data[] = [
                    'blog_id' => $v['blog_id'],
                    'author_id' => $v['author_id'],
                    'channel_id' => $v['channel_id'],
                    'title' => $v['title'],
                    'author_name' => isset($authorInfo['author_name']) ? $authorInfo['author_name'] : '',
                    'created_at' => DataService::getInstance()->_handleCreateTimeTwo($v['created_at'], 1),
                    'blog_likes' => $likeService->getBlogLikeCount($v['blog_id'], $sys)['count'] + $v['blog_likes_fake'],
                    'thumbnail_url' => replaceUrl(replaceUrl($v['thumbnail_url']), 'key_200'),
                    'is_recommend' => $v['is_recommend']
                ];

            }
        }

        $pageTotal = (int)ceil($result['total'] / (int)$context->page_count);
        $page_info = [
            'total' => $result['total'],//总记录
            'page_no' => (int)$context->page_no,//页数
            'page_count' => (int)$context->page_count,//每页显示数量
            'page_total' => $pageTotal,//总页数
        ];

        return [
            'page_info' => $page_info,
            'list' => $data,
        ];
    }

    /**
     * 获取关注的作者文章列表
     */
    public function getMyFollowAuthorArticleList($userId, $sys, $page = 1, $pageCount = 10)
    {
        //获取关注的作者
        $service = new FollowService();
        $authorIdArr = $service->getMyLikeAuthorIdList($userId, $sys);

        //获取文章列表
        $blogModel = new BlogModel();
        $result = $blogModel->getBlogInstanceByAuthorIdListByPager($authorIdArr, BlogModel::TYPE_AUTHOR_ARTICLE, $sys, $page, $pageCount);
        $blogInfo = $result['data'];

        //获取文章作者
        $authorService = new AuthorService();
        $authorIdArr = array_column($blogInfo, 'author_id');
        $authorInfo = $authorService->getAuthorInfoByIdArr($authorIdArr, $sys);
        $likeService = new LikeService();

        $data = [];
        foreach ($blogInfo as $value) {
            //获取文章点赞数量
            $likeCount = $likeService->getBlogLikeCount($value['blog_id'], $sys);
            $likeCount = $likeCount['count'];
            $data[] = [
                'blog_id' => $value['blog_id'],
                'author_id' => $value['author_id'],
                'channel_id' => $value['channel_id'],
                'title' => $value['title'],
                'author_name' => isset($authorInfo[$value['author_id']]) ? $authorInfo[$value['author_id']]['author_name'] : '',
                'created_at' => DataService::getInstance()->_handleCreateTimeTwo(date('Y-m-d H:i:s', $value['created_at'])),
                'blog_likes' => $value['blog_likes_fake'] + $likeCount,
                'thumbnail_url' => replaceUrl(replaceUrl($value['thumbnail_url']), 'key_200'),
                'is_recommend' => $value['is_recommend']
            ];
        }
        $pageTotal = (int)ceil($result['total'] / (int)$pageCount);
        $page_info = [
            'total' => $result['total'],//总记录
            'page_no' => (int)$page,//页数
            'page_count' => (int)$pageCount,//每页显示数量
            'page_total' => $pageTotal,//总页数
        ];

        return [
            'page_info' => $page_info,
            'list' => $data,
        ];
        return [
            'data' => $data,
        ];
    }

}