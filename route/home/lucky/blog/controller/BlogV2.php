<?php

namespace app\lucky\blog\controller;

use app\common\BaseController;
use app\common\DataValidate;
use app\lucky\blog\service\BlogInstanceServiceV2;
use app\lucky\blog\context\BlogV2ListContext;
use app\lucky\blog\validate\BlogV2Validate;
use think\App;
use think\Request;

class BlogV2 extends BaseController
{
    //不需登入
    protected $authCheck = false;
    protected $loginCheck = false;

    public $service = null;
    public $sys = '';

    public function __construct(App $app = null, Request $request = null)
    {
        parent::__construct($app, $request);
        $this->service = new BlogInstanceServiceV2();
        $this->sys = \session('sys') ? \session('sys') : 343;
    }

    /**
     * @description 列表
     * 路由：可访问 sports/blog/ist
     */
    public function getList()
    {
        $postData = $this->request->post();
        $valiData = BlogV2Validate::getInstance()->checkPager($postData);
        if ($valiData["code"] != _MSG_SYSTEM_SUCCESS) {
            $this->_jsonReturnV2($valiData["code"], '', $valiData["msg"]);
        }

        $context = new BlogV2ListContext($valiData['data']);
        $data = $this->service->getList($context, $this->sys);

        $this->_jsonReturnV2(0, $data, 'success');
    }

    /**
     * @description 用户关注资讯列表
     * 路由：可访问 sports/blog/user_follow_list
     */
    public function getUserFollowList()
    {
        $postData = $this->request->post();
        $valiData = DataValidate::getInstance()->checkPager($postData);
        if ($valiData["code"] != _MSG_SYSTEM_SUCCESS) {
            $this->_jsonReturnV2($valiData["code"], '', $valiData["msg"]);
        }

        $postData = $valiData['data'];
        $page = isset($postData['page_no']) ? $postData['page_no'] : 1;
        $pageCount = isset($postData['page_count']) ? $postData['page_count'] : 10;

        //获取当前登录的用户id
        $dataUser = $this->checkLoginUser();
        if (isset($dataUser['user_id'])) {
            $userId = $this->checkLoginUser()['user_id'];
        }else{
            $this->_jsonReturnV2(_MSG_NOT_LOGIN, '', '用户还未登录');
        }

        $data = $this->service->getMyFollowAuthorArticleList($userId, $this->sys, $page, $pageCount);

        $this->_jsonReturnV2(0, $data, 'success');
    }


}