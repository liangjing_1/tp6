<?php
/**
 * 资讯
 */

namespace app\lucky\blog\validate;

use app\lucky\common\InstanceTrait;
use think\Validate;

class BlogV2Validate
{
    use InstanceTrait;

    public function checkPager($data)
    {
        foreach ($data as $key => $value) {
            if (!is_array($value)) {
                $data[$key] = htmlentities($value);
            }
        }

        $rule = [
            "page_no" => "number|gt:0",
            "page_count" => "number|between:1,50",
            "is_recommend" => "number|between:0,1",
            "channel_id" => "number|gt"
        ];
        $msg = [
            "page_no.number" => "页码必须是数字",
            "page_no.gt" => "页码必须大于0",
            "page_count.number" => "每页数量必须是数字",
            "page_count.between" => "每页数量必须在1-50之间",
            "is_recommend.number" => "推荐得是数字",
            "is_recommend.between" => "推荐必须在（0-1）之间",
            "channel_id.number" => "推荐得是数字",
            "channel_id.gt" => "页码必须大于0"
        ];

        $validate = Validate::make($rule, $msg);
        $result = $validate->check($data);

        if (!$result) {
            return ["code" => _MSG_INVALID_CLIENT_PARAM, "msg" => $validate->getError()];
        }
        return ["code" => _MSG_SYSTEM_SUCCESS, "msg" => "校验成功", "data" => $data];
    }

}