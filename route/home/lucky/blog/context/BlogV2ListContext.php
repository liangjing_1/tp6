<?php
/**
 * 资讯列表参数格式化
 */

namespace app\lucky\blog\context;

use app\lucky\common\Context;

class BlogV2ListContext extends Context
{
    //页码
    public $page_no;
    //页数
    public $page_count;
    //推荐
    public $is_recommend;
    //频道ID
    public $channel_id;

}