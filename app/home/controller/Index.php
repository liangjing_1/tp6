<?php

namespace app\home\controller;

class Index
{
    public function index()
    {
        return 'hi';
    }

    public function test()
    {
        return 'test';
    }

    public function hello(string $name)
    {
        return 'Hello,'.$name;
    }
}