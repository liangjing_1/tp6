<?php


namespace app\home\controller;


use app\home\service\SocketService;
use think\facade\Db;
use think\facade\Queue;

class Owen
{
    public function test()
    {
        return view('owen/test');
    }

    public function websocket()
    {
        $sock = new SocketService();
        $sock->run();
    }

    public function queue()
    {
        $data = ['id' => 1, 'type' => 2];
        $jobHandlerClassName = 'app\queue\EventWormMatchQueue';
        $jobQueueName = "eventWormMatchQueue";
        $isPushed = Queue::push($jobHandlerClassName, $data, $jobQueueName);
        return $isPushed;
    }


    public function hello(string $name)
    {
        return 'Hello,' . $name;
    }
}