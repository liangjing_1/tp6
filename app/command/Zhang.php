<?php
declare (strict_types=1);

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class Zhang extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('zhang')
            ->setDescription('the zhang command');
    }

    protected function execute(Input $input, Output $output)
    {
        $redis = new \Redis();
        $redis->connect('127.0.0.1', 6379);
        $redis->auth('Owen.123tx'); //密码验证
        $key = 'ygkj';
        $redis->incr($key);
        $redis->expire($key, 600);
        // 指令输出
        $output->writeln('zhang');
    }
}
